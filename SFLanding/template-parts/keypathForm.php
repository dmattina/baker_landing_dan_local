<?php
/**
 * Keypath Form
 */
?>
<form class="keypath" method=get action="http://webservices.keypathpartners.com/ilm/default.ashx">
<input type='text' name='firstname' id='firstname'  placeholder="First Name" length=30>
<input type='text' name='lastname' id='lastname' placeholder="Last Name" length=30>
<input type='text' name='email' id='email' placeholder="Email" length=30>
<input type='text' name='dayphone' id='dayphone' placeholder="Phone" length=30>
<input type='text' name='address' id='address' placeholder="Address" length=30>
<input type='text' name='city' id='city' placeholder="City" length=30>
<select name="state">
<option selected disabled>State</option>
<option value="AL">Alabama</option>
<option value="AK">Alaska</option>
<option value="AZ">Arizona</option>
<option value="AR">Arkansas</option>
<option value="CA">California</option>
<option value="CO">Colorado</option>
<option value="CT">Connecticut</option>
<option value="DE">Delaware</option>
<option value="DC">Dist. of Col.</option>
<option value="FL">Florida</option>
<option value="GA">Georgia</option>
<option value="HI">Hawaii</option>
<option value="ID">Idaho</option>
<option value="IL">Illinois</option>
<option value="IN">Indiana</option>
<option value="IA">Iowa</option>
<option value="KS">Kansas</option>
<option value="KY">Kentucky</option>
<option value="LA">Louisiana</option>
<option value="ME">Maine</option>
<option value="MD">Maryland</option>
<option value="MA">Massachusetts</option>
<option value="MI">Michigan</option>
<option value="MN">Minnesota</option>
<option value="MS">Mississippi</option>
<option value="MO">Missouri</option>
<option value="MT">Montana</option>
<option value="NE">Nebraska</option>
<option value="NV">Nevada</option>
<option value="NH">New Hampshire</option>
<option value="NJ">New Jersey</option>
<option value="NM">New Mexico</option>
<option value="NY">New York</option>
<option value="NC">North Carolina</option>
<option value="ND">North Dakota</option>
<option value="OH">Ohio</option>
<option value="OK">Oklahoma</option>
<option value="OR">Oregon</option>
<option value="PA">Pennsylvania</option>
<option value="RI">Rhode Island</option>
<option value="SC">South Carolina</option>
<option value="SD">South Dakota</option>
<option value="TN">Tennessee</option>
<option value="TX">Texas</option>
<option value="UT">Utah</option>
<option value="VT">Vermont</option>
<option value="VA">Virginia</option>
<option value="WA">Washington</option>
<option value="WV">West Virginia</option>
<option value="WI">Wisconsin</option>
<option value="WY">Wyoming</option>
</select>
<input type='text' name='zip' id='zip' placeholder="Zip Code" length=30>

<select name="LocationID" id="LocationID">
<option selected disabled>Location of interest</option>
<option value="42975">Baker College - Allen Park</option>
<option value="42977">Baker College - Auburn Hills</option>
<option value="42978">Baker College - Cadillac</option>
<option value="42981">Baker College - Cass City</option>
<option value="42982">Baker College - Clinton Township</option>
<option value="42983">Baker College - Coldwater</option>
<option value="42984">Baker College - Flint</option>
<option value="42985">Baker College - Fremont</option>
<option value="42986">Baker College - Jackson</option>
<option value="42987">Baker College - Muskegon</option>
<option value="42988">Baker College - Owosso</option>
<option value="42989">Baker College - Port Huron</option>
<option value="42990">Baker College - Reading</option>
<option value="1286">Baker College Online</option>
</select>



<!--            Begin Criciulum                 -->



<select name='CurriculumID' id='Reading' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582332'>Business Administration - Associate</option>
<option value='582721'>Human Services - Associate</option>
<option value='582610'>Medical Assistant</option>
</select>

<select name='CurriculumID' id='PortHuron' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582357'>Business Administration - Accelerated Program</option>
<option value='582331'>Business Administration - Associate</option>
<option value='582700'>Criminal Justice - Associate</option>
<option value='582733'>Criminal Justice - Bachelor</option>
<option value='582662'>Health Services Administration</option>
<option value='582720'>Human Services - Associate</option>
<option value='582752'>Human Services - Bachelor</option>
<option value='582770'>Information Technology - Associate</option>
<option value='582435'>Information Technology and Security</option>
<option value='582379'>Management</option>
<option value='582597'>Medical Administrative Specialist</option>
<option value='582609'>Medical Assistant</option>
</select>


<select name='CurriculumID' id='Owosso' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582319'>Accounting - Associate</option>
<option value='582346'>Accounting - Bachelor</option>
<option value='582535'>Agriculture Technology</option>
<option value='582543'>Automotive Services Technology - Associate</option>
<option value='582516'>Automotive Services Technology - Certificate</option>
<option value='582544'>Automotive Services Technology - MOPAR CAP</option>
<option value='582356'>Business Administration - Accelerated Program</option>
<option value='582330'>Business Administration - Associate</option>
<option value='582399'>Computer Programming</option>
<option value='582411'>Computer Science</option>
<option value='582699'>Criminal Justice - Associate</option>
<option value='582732'>Criminal Justice - Bachelor</option>
<option value='582587'>Diagnostic Medical Sonography</option>
<option value='582550'>Diesel Service Technology - Associate</option>
<option value='582521'>Diesel Service Technology - Certificate</option>
<option value='582710'>Early Childhood Education - Associate</option>
<option value='582742'>Early Childhood Education - Bachelor</option>
<option value='582478'>Early Childhood ZA to ZS - Additional Endorsement</option>
<option value='582481'>Early Childhood ZS (General & Special Education) - Additional Endorsement</option>
<option value='582466'>Elementary Education - Level Change</option>
<option value='582463'>Elementary Education and Early Childhood Education - Level Change</option>
<option value='582484'>Elementary Language Arts - Additional Endorsement</option>
<option value='582487'>Elementary Mathematics - Additional Endorsement</option>
<option value='582491'>Elementary Social Studies - Additional Endorsement</option>
<option value='582494'>Elementary Social Studies - Additional Endorsement</option>
<option value='582442'>Elementary Teacher Preparation Early Childhood ZS (General & Special Education)</option>
<option value='582445'>Elementary Teacher Preparation Language Arts</option>
<option value='582448'>Elementary Teacher Preparation Mathematics</option>
<option value='582451'>Elementary Teacher Preparation Social Studies</option>
<option value='582661'>Health Services Administration</option>
<option value='582366'>Human Resource Management - Bachelor</option>
<option value='582719'>Human Services - Associate</option>
<option value='582751'>Human Services - Bachelor</option>
<option value='582769'>Information Technology - Associate</option>
<option value='582434'>Information Technology and Security</option>
<option value='582725'>Law Enforcement Academy (Police) - Associate</option>
<option value='582755'>Law Enforcement Academy (Polilce) - Bachelor</option>
<option value='582378'>Management</option>
<option value='582387'>Marketing - Bachelor</option>
<option value='582608'>Medical Assistant</option>
<option value='582675'>Nursing (Pre-licensure)</option>
<option value='582620'>Occupational Therapy Assistant</option>
<option value='582635'>Radiologic Technology</option>
<option value='582497'>Secondary English - Additional Endorsement</option>
<option value='582469'>Secondary English - Level Change</option>
<option value='582503'>Secondary Mathematics - Additional Endorsement</option>
<option value='582472'>Secondary Mathematics - Level Change</option>
<option value='582507'>Secondary Social Studies - Additional Endorsement</option>
<option value='582475'>Secondary Social Studies - Level Change</option>
<option value='582454'>Secondary Teacher Preparation English and Mathematics - Double Major</option>
<option value='582457'>Secondary Teacher Preparation English and Social Studies - Double Major</option>
<option value='582460'>Secondary Teacher Preparation Mathematics and Social Studies - Double Major</option>
<option value='582649'>Vascular Ultrasound Technology</option>
<option value='582529'>Welding - Certificate</option>
</select>

<select name='CurriculumID' id='Muskegon' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582318'>Accounting - Associate</option>
<option value='582345'>Accounting - Bachelor</option>
<option value='582355'>Business Administration - Accelerated Program</option>
<option value='582329'>Business Administration - Associate</option>
<option value='582549'>Computer Aided Design</option>
<option value='582398'>Computer Programming</option>
<option value='582410'>Computer Science</option>
<option value='582689'>Correctional Studies</option>
<option value='582698'>Criminal Justice - Associate</option>
<option value='582731'>Criminal Justice - Bachelor</option>
<option value='582406'>Digital Media Design - Associate</option>
<option value='582418'>Digital Media Design - Bachelor</option>
<option value='582709'>Early Childhood Education - Associate</option>
<option value='582741'>Early Childhood Education - Bachelor</option>
<option value='582477'>Early Childhood ZA to ZS - Additional Endorsement</option>
<option value='582480'>Early Childhood ZS (General & Special Education) - Additional Endorsement</option>
<option value='582465'>Elementary Education - Level Change</option>
<option value='582462'>Elementary Education and Early Childhood Education - Level Change</option>
<option value='582483'>Elementary Language Arts - Additional Endorsement</option>
<option value='582486'>Elementary Mathematics - Additional Endorsement</option>
<option value='582490'>Elementary Social Studies - Additional Endorsement</option>
<option value='582493'>Elementary Social Studies - Additional Endorsement</option>
<option value='582441'>Elementary Teacher Preparation Early Childhood ZS (General & Special Education)</option>
<option value='582444'>Elementary Teacher Preparation Language Arts</option>
<option value='582447'>Elementary Teacher Preparation Mathematics</option>
<option value='582450'>Elementary Teacher Preparation Social Studies</option>
<option value='582571'>Emergency Medical Technician - Basic</option>
<option value='582574'>Emergency Medical Technician - Paramedic</option>
<option value='582359'>Entrepreneurship</option>
<option value='582660'>Health Services Administration</option>
<option value='582365'>Human Resource Management - Bachelor</option>
<option value='582718'>Human Services - Associate</option>
<option value='582750'>Human Services - Bachelor</option>
<option value='582425'>Information Systems - Bachelor</option>
<option value='582768'>Information Technology - Associate</option>
<option value='582433'>Information Technology and Security</option>
<option value='582377'>Management</option>
<option value='582386'>Marketing - Bachelor</option>
<option value='582596'>Medical Administrative Specialist</option>
<option value='582607'>Medical Assistant</option>
<option value='582616'>Medical Insurance Specialist</option>
<option value='582674'>Nursing (Pre-licensure)</option>
<option value='582619'>Occupational Therapy Assistant</option>
<option value='582337'>Paralegal</option>
<option value='582627'>Pharmacy Technician</option>
<option value='582631'>Physical Therapist Assistant</option>
<option value='582634'>Radiologic Technology</option>
<option value='582496'>Secondary English - Additional Endorsement</option>
<option value='582468'>Secondary English - Level Change</option>
<option value='582502'>Secondary Mathematics - Additional Endorsement</option>
<option value='582471'>Secondary Mathematics - Level Change</option>
<option value='582506'>Secondary Social Studies - Additional Endorsement</option>
<option value='582474'>Secondary Social Studies - Level Change</option>
<option value='582453'>Secondary Teacher Preparation English and Mathematics - Double Major</option>
<option value='582456'>Secondary Teacher Preparation English and Social Studies - Double Major</option>
<option value='582459'>Secondary Teacher Preparation Mathematics and Social Studies - Double Major</option>
<option value='582577'>Sterile Processing Technician</option>
<option value='582642'>Surgical Technology</option>
<option value='582647'>Therapeutic Massage - Associate</option>
<option value='582582'>Therapeutic Massage - Certificate</option>
</select>

<select name='CurriculumID' id='Jackson' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582317'>Accounting - Associate</option>
<option value='582344'>Accounting - Bachelor</option>
<option value='582533'>Advanced Manufacturing Technology</option>
<option value='582354'>Business Administration - Accelerated Program</option>
<option value='582328'>Business Administration - Associate</option>
<option value='582683'>CDA Academic Foundations</option>
<option value='582520'>CNC Machinist</option>
<option value='582548'>Computer Aided Design</option>
<option value='582397'>Computer Programming</option>
<option value='582688'>Correctional Studies</option>
<option value='582697'>Criminal Justice - Associate</option>
<option value='582730'>Criminal Justice - Bachelor</option>
<option value='582415'>Cyber Defense</option>
<option value='582708'>Early Childhood Education - Associate</option>
<option value='582740'>Early Childhood Education - Bachelor</option>
<option value='582591'>Health Information Technology</option>
<option value='582659'>Health Services Administration</option>
<option value='582717'>Human Services - Associate</option>
<option value='582749'>Human Services - Bachelor</option>
<option value='582552'>Industrial Technology</option>
<option value='582424'>Information Systems - Bachelor</option>
<option value='582767'>Information Technology - Associate</option>
<option value='582432'>Information Technology and Security</option>
<option value='582376'>Management</option>
<option value='582567'>Mechanical Engineering</option>
<option value='582556'>Mechanical Technology</option>
<option value='582595'>Medical Administrative Specialist</option>
<option value='582606'>Medical Assistant</option>
<option value='582615'>Medical Insurance Specialist</option>
<option value='582673'>Nursing (Pre-licensure)</option>
<option value='582336'>Paralegal</option>
<option value='582667'>Practical Nurse</option>
<option value='582665'>Radiation Therapy</option>
<option value='582691'>Special Needs Paraprofesisonal/Behavioral Technician</option>
<option value='582641'>Surgical Technology</option>
<option value='582646'>Therapeutic Massage - Associate</option>
<option value='582581'>Therapeutic Massage - Certificate</option>
</select>

<select name='CurriculumID' id='Fremont' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582316'>Accounting - Associate</option>
<option value='582353'>Business Administration - Accelerated Program</option>
<option value='582327'>Business Administration - Associate</option>
<option value='582570'>Emergency Medical Technician - Basic</option>
<option value='582658'>Health Services Administration</option>
<option value='582605'>Medical Assistant</option>
</select>

<select name='CurriculumID' id='Flint' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582315'>Accounting - Associate</option>
<option value='582343'>Accounting - Bachelor</option>
<option value='582532'>Advanced Manufacturing Technology</option>
<option value='582537'>Autobody Technician - Associate</option>
<option value='582511'>Autobody Technician - Certificate</option>
<option value='582538'>Automotive Restoration Technology</option>
<option value='582542'>Automotive Services Technology - Associate</option>
<option value='582515'>Automotive Services Technology - Certificate</option>
<option value='582352'>Business Administration - Accelerated Program</option>
<option value='582326'>Business Administration - Associate</option>
<option value='582682'>CDA Academic Foundations</option>
<option value='582562'>Civil Engineering</option>
<option value='582519'>CNC Machinist</option>
<option value='582547'>Computer Aided Design</option>
<option value='582396'>Computer Programming</option>
<option value='582409'>Computer Science</option>
<option value='582687'>Correctional Studies</option>
<option value='582696'>Criminal Justice - Associate</option>
<option value='582729'>Criminal Justice - Bachelor</option>
<option value='582414'>Cyber Defense</option>
<option value='582405'>Digital Media Design - Associate</option>
<option value='582707'>Early Childhood Education - Associate</option>
<option value='582739'>Early Childhood Education - Bachelor</option>
<option value='582564'>Electrical Engineering</option>
<option value='582590'>Health Information Technology</option>
<option value='582657'>Health Services Administration</option>
<option value='582364'>Human Resource Management - Bachelor</option>
<option value='582716'>Human Services - Associate</option>
<option value='582748'>Human Services - Bachelor</option>
<option value='582766'>Information Technology - Associate</option>
<option value='582431'>Information Technology and Security</option>
<option value='582722'>Interpreter Training</option>
<option value='582724'>Law Enforcement Academy (Police) - Associate</option>
<option value='582754'>Law Enforcement Academy (Polilce) - Bachelor</option>
<option value='582375'>Management</option>
<option value='582385'>Marketing - Bachelor</option>
<option value='582566'>Mechanical Engineering</option>
<option value='582555'>Mechanical Technology</option>
<option value='582594'>Medical Administrative Specialist</option>
<option value='582604'>Medical Assistant</option>
<option value='582614'>Medical Insurance Specialist</option>
<option value='582672'>Nursing (Pre-licensure)</option>
<option value='582666'>Occupational Therapy - MOT</option>
<option value='582622'>Orthotic/Prosthetic Technology</option>
<option value='582626'>Pharmacy Technician</option>
<option value='582558'>Photonics and Laser Technology</option>
<option value='582630'>Physical Therapist Assistant</option>
<option value='582632'>Polysomnographic Technology</option>
<option value='582664'>Pre-Occupational Therapy</option>
<option value='582690'>Special Needs Paraprofesisonal/Behavioral Technician</option>
<option value='582576'>Sterile Processing Technician</option>
<option value='582640'>Surgical Technology</option>
<option value='582524'>Truck Driving</option>
<option value='582652'>Veterinary Technology</option>
<option value='582561'>Welding - Associate</option>
<option value='582528'>Welding - Certificate</option>
</select>

<select name='CurriculumID' id='Coldwater' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582314'>Accounting - Associate</option>
<option value='582342'>Accounting - Bachelor</option>
<option value='582706'>Early Childhood Education - Associate</option>
<option value='582765'>Information Technology - Associate</option>
<option value='582374'>Management</option>
<option value='582603'>Medical Assistant</option>
<option value='582613'>Medical Insurance Specialist</option>
</select>


<select name='CurriculumID' id='ClintonTownship' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582313'>Accounting - Associate</option>
<option value='582341'>Accounting - Bachelor</option>
<option value='582536'>Autobody Technician - Associate</option>
<option value='582510'>Autobody Technician - Certificate</option>
<option value='582541'>Automotive Services Technology - Associate</option>
<option value='582514'>Automotive Services Technology - Certificate</option>
<option value='582351'>Business Administration - Accelerated Program</option>
<option value='582325'>Business Administration - Associate</option>
<option value='582681'>CDA Academic Foundations</option>
<option value='582395'>Computer Programming</option>
<option value='582408'>Computer Science</option>
<option value='582686'>Correctional Studies</option>
<option value='582695'>Criminal Justice - Associate</option>
<option value='582728'>Criminal Justice - Bachelor</option>
<option value='582413'>Cyber Defense</option>
<option value='582585'>Dental Hygiene</option>
<option value='582404'>Digital Media Design - Associate</option>
<option value='582705'>Early Childhood Education - Associate</option>
<option value='582738'>Early Childhood Education - Bachelor</option>
<option value='582476'>Early Childhood ZA to ZS - Additional Endorsement</option>
<option value='582479'>Early Childhood ZS (General & Special Education) - Additional Endorsement</option>
<option value='582464'>Elementary Education - Level Change</option>
<option value='582461'>Elementary Education and Early Childhood Education - Level Change</option>
<option value='582482'>Elementary Language Arts - Additional Endorsement</option>
<option value='582485'>Elementary Mathematics - Additional Endorsement</option>
<option value='582489'>Elementary Social Studies - Additional Endorsement</option>
<option value='582492'>Elementary Social Studies - Additional Endorsement</option>
<option value='582440'>Elementary Teacher Preparation Early Childhood ZS (General & Special Education)</option>
<option value='582443'>Elementary Teacher Preparation Language Arts</option>
<option value='582446'>Elementary Teacher Preparation Mathematics</option>
<option value='582449'>Elementary Teacher Preparation Social Studies</option>
<option value='582569'>Emergency Medical Technician - Basic</option>
<option value='582573'>Emergency Medical Technician - Paramedic</option>
<option value='582419'>Game Software Development</option>
<option value='582589'>Health Information Technology</option>
<option value='582656'>Health Services Administration</option>
<option value='582522'>Heating, Ventilation, Air Conditioning Technology</option>
<option value='582363'>Human Resource Management - Bachelor</option>
<option value='582715'>Human Services - Associate</option>
<option value='582747'>Human Services - Bachelor</option>
<option value='582423'>Information Systems - Bachelor</option>
<option value='582764'>Information Technology - Associate</option>
<option value='582430'>Information Technology and Security</option>
<option value='582723'>Law Enforcement Academy (Police) - Associate</option>
<option value='582753'>Law Enforcement Academy (Polilce) - Bachelor</option>
<option value='582373'>Management</option>
<option value='582384'>Marketing - Bachelor</option>
<option value='582593'>Medical Administrative Specialist</option>
<option value='582602'>Medical Assistant</option>
<option value='582671'>Nursing (Pre-licensure)</option>
<option value='582625'>Pharmacy Technician</option>
<option value='582633'>Radiologic Technology</option>
<option value='582495'>Secondary English - Additional Endorsement</option>
<option value='582467'>Secondary English - Level Change</option>
<option value='582501'>Secondary Mathematics - Additional Endorsement</option>
<option value='582470'>Secondary Mathematics - Level Change</option>
<option value='582505'>Secondary Social Studies - Additional Endorsement</option>
<option value='582473'>Secondary Social Studies - Level Change</option>
<option value='582452'>Secondary Teacher Preparation English and Mathematics - Double Major</option>
<option value='582455'>Secondary Teacher Preparation English and Social Studies - Double Major</option>
<option value='582458'>Secondary Teacher Preparation Mathematics and Social Studies - Double Major</option>
<option value='582639'>Surgical Technology</option>
<option value='582645'>Therapeutic Massage - Associate</option>
<option value='582580'>Therapeutic Massage - Certificate</option>
<option value='582651'>Veterinary Technology</option>
</select>

<select name='CurriculumID' id='CassCity' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582312'>Accounting - Associate</option>
<option value='582324'>Business Administration - Associate</option>
<option value='582704'>Early Childhood Education - Associate</option>
<option value='582714'>Human Services - Associate</option>
<option value='582746'>Human Services - Bachelor</option>
<option value='582763'>Information Technology - Associate</option>
<option value='582372'>Management</option>
<option value='582601'>Medical Assistant</option>
<option value='582624'>Pharmacy Technician</option>
<option value='582527'>Welding - Certificate</option>
</select>

<select name='CurriculumID' id='Cadillac' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582311'>Accounting - Associate</option>
<option value='582340'>Accounting - Bachelor</option>
<option value='582531'>Advanced Manufacturing Technology</option>
<option value='582534'>Agriculture Technology</option>
<option value='582540'>Automotive Services Technology - Associate</option>
<option value='582513'>Automotive Services Technology - Certificate</option>
<option value='582350'>Business Administration - Accelerated Program</option>
<option value='582323'>Business Administration - Associate</option>
<option value='582518'>CNC Machinist</option>
<option value='582546'>Computer Aided Design</option>
<option value='582685'>Correctional Studies</option>
<option value='582694'>Criminal Justice - Associate</option>
<option value='582727'>Criminal Justice - Bachelor</option>
<option value='582403'>Digital Media Design - Associate</option>
<option value='582703'>Early Childhood Education - Associate</option>
<option value='582737'>Early Childhood Education - Bachelor</option>
<option value='582568'>Emergency Medical Technician - Basic</option>
<option value='582572'>Emergency Medical Technician - Paramedic</option>
<option value='582655'>Health Services Administration</option>
<option value='582362'>Human Resource Management - Bachelor</option>
<option value='582713'>Human Services - Associate</option>
<option value='582745'>Human Services - Bachelor</option>
<option value='582762'>Information Technology - Associate</option>
<option value='582429'>Information Technology and Security</option>
<option value='582371'>Management</option>
<option value='582383'>Marketing - Bachelor</option>
<option value='582557'>Mechatronics</option>
<option value='582592'>Medical Administrative Specialist</option>
<option value='582600'>Medical Assistant</option>
<option value='582612'>Medical Insurance Specialist</option>
<option value='582670'>Nursing (Pre-licensure)</option>
<option value='582638'>Surgical Technology</option>
<option value='582644'>Therapeutic Massage - Associate</option>
<option value='582579'>Therapeutic Massage - Certificate</option>
<option value='582523'>Truck Driving</option>
<option value='582650'>Veterinary Technology</option>
<option value='582560'>Welding - Associate</option>
<option value='582526'>Welding - Certificate</option>
</select>

<select name='CurriculumID' id='AuburnHills' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582310'>Accounting - Associate</option>
<option value='582339'>Accounting - Bachelor</option>
<option value='582539'>Automotive Services Technology - Associate</option>
<option value='582349'>Business Administration - Accelerated Program</option>
<option value='582322'>Business Administration - Associate</option>
<option value='582583'>Cardiac Sonography</option>
<option value='582680'>CDA Academic Foundations</option>
<option value='582584'>Coding Specialist</option>
<option value='582545'>Computer Aided Design</option>
<option value='582394'>Computer Programming</option>
<option value='582693'>Criminal Justice - Associate</option>
<option value='582586'>Diagnostic Medical Sonography</option>
<option value='582402'>Digital Media Design - Associate</option>
<option value='582417'>Digital Media Design - Bachelor</option>
<option value='582702'>Early Childhood Education - Associate</option>
<option value='582736'>Early Childhood Education - Bachelor</option>
<option value='582654'>Health Services Administration</option>
<option value='582361'>Human Resource Management - Bachelor</option>
<option value='582712'>Human Services - Associate</option>
<option value='582744'>Human Services - Bachelor</option>
<option value='582551'>Industrial Technology</option>
<option value='582422'>Information Systems - Bachelor</option>
<option value='582761'>Information Technology - Associate</option>
<option value='582428'>Information Technology and Security</option>
<option value='582553'>Interior Design - Associate</option>
<option value='582565'>Interior Design - Bachelor</option>
<option value='582368'>Legal Studies</option>
<option value='582370'>Management</option>
<option value='582382'>Marketing - Bachelor</option>
<option value='582599'>Medical Assistant</option>
<option value='582611'>Medical Insurance Specialist</option>
<option value='582669'>Nursing (Pre-licensure)</option>
<option value='582335'>Paralegal</option>
<option value='582575'>Phlebotomy Technician</option>
<option value='582629'>Physical Therapist Assistant</option>
<option value='582636'>Respiratory Care</option>
<option value='582648'>Vascular Ultrasound Technology</option>
<option value='582559'>Welding - Associate</option>
<option value='582525'>Welding - Certificate</option>
</select>

<select name='CurriculumID' id='AllenPark' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582309'>Accounting - Associate</option>
<option value='582338'>Accounting - Bachelor</option>
<option value='582530'>Advanced Manufacturing Technology</option>
<option value='582512'>Automotive Services Technology - Certificate</option>
<option value='582348'>Business Administration - Accelerated Program</option>
<option value='582321'>Business Administration - Associate</option>
<option value='582679'>CDA Academic Foundations</option>
<option value='582517'>CNC Machinist</option>
<option value='582393'>Computer Programming</option>
<option value='582563'>Construction Management</option>
<option value='582684'>Correctional Studies</option>
<option value='582692'>Criminal Justice - Associate</option>
<option value='582726'>Criminal Justice - Bachelor</option>
<option value='582412'>Cyber Defense</option>
<option value='582401'>Digital Media Design - Associate</option>
<option value='582701'>Early Childhood Education - Associate</option>
<option value='582735'>Early Childhood Education - Bachelor</option>
<option value='582588'>Health Information Technology</option>
<option value='582653'>Health Services Administration</option>
<option value='582360'>Human Resource Management - Bachelor</option>
<option value='582711'>Human Services - Associate</option>
<option value='582743'>Human Services - Bachelor</option>
<option value='582421'>Information Systems - Bachelor</option>
<option value='582760'>Information Technology - Associate</option>
<option value='582427'>Information Technology and Security</option>
<option value='582369'>Management</option>
<option value='582381'>Marketing - Bachelor</option>
<option value='582554'>Mechanical Technology</option>
<option value='582598'>Medical Assistant</option>
<option value='582617'>Medical Laboratory Technician</option>
<option value='582668'>Nursing</option>
<option value='582618'>Occupational Therapy Assistant</option>
<option value='582621'>Opticianry</option>
<option value='582334'>Paralegal</option>
<option value='582623'>Pharmacy Technician</option>
<option value='582628'>Physical Therapist Assistant</option>
<option value='582637'>Surgical Technology</option>
<option value='582643'>Therapeutic Massage - Associate</option>
<option value='582578'>Therapeutic Massage - Certificate</option>
</select>

<select name='CurriculumID' id='Online' style=" display: none;">
<option selected value="">Program of Interest:</option>
<option value='582347'>Accounting - Bachelor</option>
<option value='17190'>Accounting - MBA</option>
<option value='582358'>Business Administration - Accelerated Program</option>
<option value='582333'>Business Administration - Associate</option>
<option value='582392'>Business Administration - DBA</option>
<option value='582390'>Business Intelligence</option>
<option value='582438'>Business Intelligence - MSIS</option>
<option value='582400'>Computer Programming</option>
<option value='7396'>Computer Science</option>
<option value='582734'>Criminal Justice - Bachelor</option>
<option value='582416'>Database Technology</option>
<option value='582508'>Educational Leadership: Higher Education - MSEE</option>
<option value='582509'>Educational Leadership: K-12 - MSEE</option>
<option value='582488'>Elementary Mathematics - Additional Endorsement</option>
<option value='582407'>Finance - Bachelor</option>
<option value='17191'>Finance - MBA</option>
<option value='582420'>Game Software Development</option>
<option value='17192'>General Business</option>
<option value='582757'>General Studies</option>
<option value='582663'>Health Services Administration</option>
<option value='582391'>Healthcare Management</option>
<option value='582367'>Human Resource Management - Bachelor</option>
<option value='17194'>Human Resource Management - MBA</option>
<option value='582759'>Industrial/Organizational Psychology - MSIOP</option>
<option value='16122'>Information Systems</option>
<option value='582426'>Information Systems - Bachelor</option>
<option value='582439'>Information Systems - MSIS</option>
<option value='582771'>Information Technology - Associate</option>
<option value='582436'>Information Technology and Security</option>
<option value='582756'>Law Enforcement Academy (Polilce) - Bachelor</option>
<option value='17195'>Leadership Studies</option>
<option value='582380'>Management</option>
<option value='582388'>Marketing - Bachelor</option>
<option value='17196'>Marketing - MBA</option>
<option value='582437'>Mobile Application Software Engineering</option>
<option value='582676'>Nursing (Post-licensure)</option>
<option value='582677'>Nursing Administration</option>
<option value='582678'>Nursing Education</option>
<option value='17758'>Project Management and Planning</option>
<option value='582758'>Psychology</option>
<option value='582504'>Secondary Mathematics - Additional Endorsement</option>
<option value='582389'>Supply Chain Management</option>
<option value='7397'>Web Development</option>
</select>



<!--            End Criciulum                 -->



<select name="gradyear">
<option selected disabled>High School Graduation Year</option>
<option value='2021'>2021</option>
<option value='2020'>2020</option>
<option value='2019'>2019</option>
<option value='2018'>2018</option>
<option value='2017'>2017</option>
<option value='2016'>2016</option>
<option value='2015'>2015</option>
<option value='2014'>2014</option>
<option value='2013'>2013</option>
<option value='2012'>2012</option>
<option value='2011'>2011</option>
<option value='2010'>2010</option>
<option value='2009'>2009</option>
<option value='2008'>2008</option>
<option value='2007'>2007</option>
<option value='2006'>2006</option>
<option value='2005'>2005</option>
<option value='2004'>2004</option>
<option value='2003'>2003</option>
<option value='2002'>2002</option>
<option value='2001'>2001</option>
<option value='2000'>2000</option>
<option value='1999'>1999</option>
<option value='1998'>1998</option>
<option value='1997'>1997</option>
<option value='1996'>1996</option>
<option value='1995'>1995</option>
<option value='1994'>1994</option>
<option value='1993'>1993</option>
<option value='1992'>1992</option>
<option value='1991'>1991</option>
<option value='1990'>1990</option>
<option value='1989'>1989</option>
<option value='1988'>1988</option>
<option value='1987'>1987</option>
<option value='1986'>1986</option>
<option value='1985'>1985</option>
<option value='1984'>1984</option>
<option value='1983'>1983</option>
<option value='1982'>1982</option>
<option value='1981'>1981</option>
<option value='1980'>1980</option>
<option value='1979'>1979</option>
<option value='1978'>1978</option>
<option value='1977'>1977</option>
<option value='1976'>1976</option>
<option value='1975'>1975</option>
<option value='1974'>1974</option>
<option value='1973'>1973</option>
<option value='1972'>1972</option>
<option value='1971'>1971</option>
<option value='1970'>1970</option>
<option value='1969'>1969</option>
<option value='1968'>1968</option>
<option value='1967'>1967</option>
<option value='1966'>1966</option>
<option value='1965'>1965</option>
<option value='1964'>1964</option>
<option value='1963'>1963</option>
<option value='1962'>1962</option>
<option value='1961'>1961</option>
<option value='1960'>1960</option>
<option value='1959'>1959</option>
<option value='1958'>1958</option>
<option value='1957'>1957</option>
<option value='1956'>1956</option>
<option value='1955'>1955</option>
<option value='1954'>1954</option>
<option value='1953'>1953</option>
<option value='1952'>1952</option>
<option value='1951'>1951</option>
<option value='1950'>1950</option>
<option value='1949'>1949</option>
<option value='1948'>1948</option>
<option value='1947'>1947</option>
<option value='1946'>1946</option>
<option value='1945'>1945</option>
<option value='1944'>1944</option>
<option value='1943'>1943</option>
<option value='1942'>1942</option>
<option value='1941'>1941</option>
<option value='1940'>1940</option>
<option value='1939'>1939</option>
<option value='1938'>1938</option>
<option value='1937'>1937</option>
<option value='1936'>1936</option>
<option value='1935'>1935</option>
<option value='1934'>1934</option>
<option value='1933'>1933</option>
<option value='1932'>1932</option>
<option value='1931'>1931</option>
<option value='1930'>1930</option>
<option value='1929'>1929</option>
<option value='1928'>1928</option>
<option value='1927'>1927</option>
<option value='1926'>1926</option>
<option value='1925'>1925</option>
<option value='1924'>1924</option>
<option value='1923'>1923</option>
<option value='1922'>1922</option>
<option value='1921'>1921</option>
</select>
<input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
<input type='hidden' placeholder="" name='FormID' id='FormID' value=7452 >
<input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7165 >
<input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >
<input type='hidden' placeholder="" name='CurriculumID' id='CurriculumID' value=582288 >
<input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='trafficdev.net'>
<input type='hidden' placeholder="" name='IsTest' id='IsTest' value='TRUE'>
<input type='submit' name='Submit'>
</form>


