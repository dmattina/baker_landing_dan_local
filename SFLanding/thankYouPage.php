<?php
/**
 * Template Name:Thank You Page
 */
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test custom post type -->

<div class="head" style="background: <?php the_field('big_slide_image'); ?> ;">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    <div id="SlideInfo">

	<div class="VertAlign">
      	<h1><?php the_field('headline'); ?></h1>
	<a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/slices/Video Ply BTN.png" alt="Play Video"></a>
	<p><?php the_field('sub-headline') ?></p>
	</div>

    </div><!-- close SlideInfo -->
    <div id="phone">
      <p style="font-family: Nexa-regular, sans-serif;"><span id="">Toll-Free:</span> <a href="tel:855-487-7888">855-487-7888</a></p>
    </div><!-- close phone-->
  </header>
</div><!-- close head -->

<?php endwhile; endif; ?>
<?php get_footer(); ?>
