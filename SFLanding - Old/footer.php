<?php

/**

 * The template for displaying the footer

 * Contains the closing of the #content div and all content after

 */

?>

<?php the_field('extratracking_code'); ?>

<script>
$('input').jvFloat();
//Dynamic Header height
    //Adjust element dimensions on resize event
    $(window).on('resize', function () {
        $('.head').css('height', $(this).height() + "px");

        formScroll();
        titleAlign();
        formSwap();
    });
    //Trigger the event
    $(window).trigger('resize');


//Follow form Js

/*var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
var fixmePosition = $('.fixme').position();
var leftmargin = $('.fixme').css('margin-left');
var width = document.getElementById('fix').offsetWidth;
var intoSize = $('.intro').height() + 45;
$('.fixme').css({marginTop: '-' + intoSize + 'px',})

  $(window).scroll(function() {                     // assign scroll event listener
  var currentScroll = $(window).scrollTop();        // get current position
  var windowSize = $(window).width();
  var formHeight= $('.fixme').height();
  var tilesHeight= $('.tiles').offset().top ;
  var stopMe = tilesHeight - formHeight;

  if (currentScroll >= (fixmeTop - 200) && windowSize > 800) {   	// apply position: fixed if you
    	var fromTop = $('.fixme').offset().top;
    	$('.fixme').css({                                 // scroll to that element or below it
		position: 'fixed',
		width: width + 'px',
		top: '200px',
		left: fixmePosition.left  + 'px',
		marginTop: '-' + intoSize + 'px',
		marginLeft: leftmargin,
		});

	} else {                               // apply position: static
	$('.fixme').css({                      // if you scroll above it
		position:'static',
		top:'auto',
		left :'auto',
   		marginTop: '-' + intoSize + 'px',
		width:'31.76157%'
		});
	}
  if (currentScroll >= (stopMe - 350) && windowSize > 800) {           // apply position: fixed if you
      	$('.fixme').css({                                 // scroll to that element or below it
          	position: 'absolute',
          	width: width + 'px',
          	top: (stopMe - 100) + 'px',
          	left: fixmePosition.left + 'px',
            	marginTop: '-' + intoSize + 'px',
      		});
	}
  });*/

function formScroll(){
   var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
var fixmePosition = $('.fixme').position();
var leftmargin = $('.fixme').css('margin-left');
var width = document.getElementById('fix').offsetWidth;
var intoSize = $('.intro').height() + 45;
$('.fixme').css({marginTop: '-' + intoSize + 'px',})

  $(window).scroll(function() {                     // assign scroll event listener
  var currentScroll = $(window).scrollTop();        // get current position
  var windowSize = $(window).width();
  var formHeight= $('.fixme').height();
  var tilesHeight= $('.tiles').offset().top ;
  var stopMe = tilesHeight - formHeight;

  if (currentScroll >= (fixmeTop - 200) && windowSize > 1000) {    // apply position: fixed if you
      var fromTop = $('.fixme').offset().top;
      $('.fixme').css({                                 // scroll to that element or below it
    position: 'fixed',
    width: width + 'px',
    top: '200px',
    left: fixmePosition.left  + 'px',
    marginTop: '-' + intoSize + 'px',
    marginLeft: leftmargin,
    });

  } else {                               // apply position: static
  $('.fixme').css({                      // if you scroll above it
    position:'static',
    top:'auto',
    left :'auto',
      marginTop: '-' + intoSize + 'px',
    width:'31.76157%'
    });
  }
  if (currentScroll >= (stopMe - 350) && windowSize > 1000) {           // apply position: fixed if you
        $('.fixme').css({                                 // scroll to that element or below it
            position: 'absolute',
            width: width + 'px',
            top: (stopMe - 100) + 'px',
            left: fixmePosition.left + 'px',
              marginTop: '-' + intoSize + 'px',
          });
  }
  });
}

function titleAlign(){
  var getHeight = $(window).height();
  var getWidth = $(window).width();
  if( getHeight <= 319 && getWidth < 765 ){
     $('.mobileForm').css('margin-top','0px');
     $('header #SlideInfo .VertAlign').css('padding-top','80px');
  }
  /*if( getWidth > 765 ){
    var titlePad = getHeight/2 - 120;
    $('header #SlideInfo .VertAlign').css('padding-top', titlePad + 'px');
  }*/
  else{
    var titlePad = getHeight/2 - 120;
    $('header #SlideInfo .VertAlign').css('padding-top', titlePad + 'px');
    $('.mobileForm').css('margin-top','-130px');
  }
  console.log (getHeight);
}

function formSwap(){
  var getWidth = $(window).width();
  if(getWidth <= 900){
    $('.mobileForm form.keypathMobile').remove();
    $('div.aside .keypath').detach().insertAfter('.mobileForm #Intro');           
  }
  else
  {
    $('.mobileForm .keypath').detach().insertAfter('div.aside #Intro'); 
  }
}

</script>

<script>
$(function(){
  var mobileTop = $('.mobileForm').offset().top;
	var mobileHeight = $('.mobileForm').height();
	var windowSize = $(window).width();
	var ShowHeightMobile = mobileTop + mobileHeight
  $(window).scroll(function(){
	   var currentScroll = $(window).scrollTop()
	    if (currentScroll > ShowHeightMobile && windowSize < 800 ) {
        $('.goToFormMobile').css('display', 'block');
		    $('.goToFormMobile').addClass("slideDown");
    	} else {
        $('.goToFormMobile').css('display', 'none');
    		}
        });
  });
</script>

<?php wp_footer(); ?>

</body>

</html>
