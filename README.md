# DO NOT CLONE DOWN. PLEASE FORK AND CLONE YOUR OWN FORKED COPY #

We use Bit bucket to manage the code repo across our devolepment team. Make and commit all changes to the Bit Bucket repo. 

Push to the Bitbucket repo:
git push origin master


We use Beanstalk as a deployment service. All of the Deployments for Baker are done through this service. The dev envryoments will be deployed automaticly, so once you commit your code to the master the system will automaticly push to the dev envyroment. Once you are ready you can log into Beanstalk (https://trafficdigitalagency.beanstalkapp.com/) and manualy push to the Production servers, BakerLandingPages > Deployments and Click deploy next to the server you are read to push to. 

To push to the Auto Deployment:
git push beanstalk master
