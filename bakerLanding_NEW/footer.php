<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 */
?>

<?php //the_field('extratracking_code'); ?>

<?php if(get_the_title() == 'Thank You'): ?>
    <script type="text/javascript">
                window._vis_opt_queue = window._vis_opt_queue || [];
                window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(200);});
    </script>
    <!-- -------------------------------------------------------------------------------
    CONFIRMATION PAGE CODE
    Be sure to replace all the variables indicated with @VARIABLE@ with real data.
    -------------------------------------------------------------------------------- -->

    <!-- BEGIN: Marin Software Tracking Script (Confirmation Page) -->
    <script type='text/javascript'>
    var _mTrack = _mTrack || [];
    _mTrack.push(['addTrans', {
        currency :'USD',
        items : [{
            orderId : '@ORDER-ID@',
            convType : 'lead',
            price :    '@PRICE@'
        }]
    }]);
    _mTrack.push(['processOrders']);
    (function() {
        var mClientId = '16680tk457738';
        var mProto = (('https:' == document.location.protocol) ? 'https://' : 'http://');
        var mHost = 'tracker.marinsm.com';
        var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
        var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr);
    })();
    </script>
    <noscript>
    <img width="1" height="1" src="https://tracker.marinsm.com/tp?act=2&cid=16680tk457738&script=no" />
    </noscript>
    <!-- END: Copyright Marin Software -->
<?php endif; ?> 

<?php if(get_the_title() != 'Thank You'): ?>
    <!-- BEGIN: Marin Software Tracking Script (Landing Page) -->
    <script type='text/javascript'>
    var _mTrack = _mTrack || [];
    _mTrack.push(['trackPage']);

    (function() {
    var mClientId = '16680tk457738';
    var mProto = (('https:' == document.location.protocol) ? 'https://' : 'http://');
    var mHost = 'tracker.marinsm.com';

    var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
    var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr);
    })();
    </script>
    <noscript>
    <img width="1" height="1" src="https://tracker.marinsm.com/tp?act=1&cid=16680tk457738&script=no" />
    </noscript>
    <!-- END: Copyright Marin Software -->
<?php endif; ?> 

  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/jquery.validate.js"></script>
  <script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/additional-methods.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/inputmask.phone.extensions.js"></script>
  <script src="<?php echo get_bloginfo('template_directory');?>/js/jquery.inputmask.js"></script>-->


<?php wp_footer(); ?>

</body>

</html>
