
$('input').jvFloat();

$(document).ready(function(){
  /* Catch Query strings and pass the same to Home page Icons link - Start */
  var queryParameters = window.location.href.split("?")[1];
  if(queryParameters != null){
    $(".gen_icons a").each(function(){
      var temphref = $(this).attr("href");
      temphref += "?";
      temphref += queryParameters;
      $(this).attr("href", temphref);
    });
  }
  /* Catch Query strings and pass the same to Home page Icons link - END */

  $('.Thank .head').css('height', $(window).height() + "px");
  console.log('height added' + $('.Thank .head').height());
  
  //$('#SlideInfo h1, .success_holder h2').append('<i> > </i>');
   $('.all_programs > h1').append(' <i>  </i>');

  // All Program in single wysiwig editor - Toggle 
  $('.all_programs > h1').click(function(e){
      var current = $(this).next('blockquote');
      $('.all_programs > blockquote').not(current).hide(100);
      $('.all_programs > h1 i').not($(this).find('i')).removeClass('open_arrow');
      $('.all_programs > h1').not($(this)).removeClass('open'); // open = Mobile  || open_arrow = desktop

      current.slideToggle();
      // open = Mobile  || open_arrow = desktop
      if($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open') ){
          $(this).find('i').removeClass('open_arrow');
          $(this).removeClass('open');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow') || ! $(this).hasClass('open') ) {
          $(this).find('i').addClass('open_arrow');
          $(this).addClass('open')
      }
      if($(window).width() < 767){
                  
      }
  });


  // Separate Program - Toggle Function
  $('.hold > h3').click(function(e){
      var current = $(this).next('section');
      $('.hold > section').not(current).hide(100);
      $('.hold > h3 i').not($(this).find('i')).removeClass('open_arrow');
      current.slideToggle();
      if($(this).find('i').hasClass('open_arrow')){
          $(this).find('i').removeClass('open_arrow');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow')) {
          $(this).find('i').addClass('open_arrow');
      }
  });

  $('.locations_holder .content .title > h3').click(function(e){
    if($(window).width() < 768){
      $('.locations_list, .additional_locations').toggle();
      $(this).find('i').toggleClass('loc_open_arrow');
    }
  })

});



    

    //Trigger the event
    $(window).trigger('resize');


//Follow form Js
$(window).on('resize', function () {
    
    $('.Thank .head').css('height', $(this).height() + "px");
    formScroll();
});




function formScroll() {
  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
  var fixmePosition = $('.fixme').position();
  var leftmargin = $('.fixme').css('margin-left');
  //var width = document.getElementById('fix').offsetWidth;
  //var intoSize = $('.intro').height() + 35;
  var intoSize = $('#intro').height();
  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
console.log(collapsePoint);

  //$(window).scroll(function() {   
  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
        var currentScroll = $(window).scrollTop();        // get current position  
        var windowSize = $(window).width();
        var formHeight= $('.fixme').height();
        /*var tilesHeight= $('.tiles').offset().top ;
        var stopMe = tilesHeight - formHeight;*/
        var tilesHeight= $('.testimonial_holder').offset().top ;
        var stopMe = tilesHeight - formHeight;

       
        if (currentScroll >= (fixmeTop - 100) && windowSize > 1000) {    // apply position: fixed if you
          
            var fromTop = $('.fixme').offset().top;
            $('.fixme').css({                                 // scroll to that element or below it
          position: 'fixed',
          top: '80px',
          left: fixmePosition.left  + 'px',
          marginTop: '-' + intoSize + 'px',
          });
        } 
        else {                               // apply position: static
        $('.fixme').css({                      // if you scroll above it
          position:'relative',
          top:'auto',
          left :'auto',
            marginTop: '-' + intoSize + 'px',
          });
        }

        if (currentScroll >= (stopMe - 100) && windowSize > 1000) {           // apply position: fixed if you
              $('.fixme').css({                                 // scroll to that element or below it
                  position: 'absolute',
                  /*width: width + 'px',*/
                  top: stopMe + 'px',
                  left: fixmePosition.left + 'px',
                    marginTop: '-' + intoSize + 'px',
                });
        }
  });
//console.log (leftmargin);
}


$(document).ready(function(){
    
    formScroll();
    $('#Intro').click(function(){
      $('form.keypath').slideToggle();
      var formState = $("form.keypath");
      var windowSize = $(window).width();

    })

    if(isMobile.any()){
    var _originalSize = $(window).width() + $(window).height();
    var _originalHeadHeight = $(window).height();

    $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
          console.log("keyboard on");
          //alert('keyboard on'); 
          //$(".head").css("height", _originalHeadHeight + "px");  
        }else{
          console.log("keyboard off");
          //alert('keyboard off');
        }
      });
    }
});







/* Mobile Keyboard On Detection for Avoid title text merging with request info form */

// Detect Mobile Device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}; 

$(document).ready(function(){
  
  

});


$.validator.addMethod('customphone', function (value, element) {
    return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
}, "Please enter a valid phone number");

$.validator.addMethod("fillProgram", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify the program you are interested in");

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

$.validator.addMethod("nonNumeric", function(value, element) {
    return this.optional(element) || !value.match(/[0-9]+/);
},"Only alphabatic characters allowed.");

$(document).ready(function() {
  // validate signup form on keyup and submit
  $(".keypath").validate({
    rules: {
      firstname: "required",
      lastname: "required",
      email: {
    required: true,
    email: true
    },
      dayphone: "customphone",
      address: "required",
      city: {
    required: true,
    nonNumeric: true
    },
      state: "required",
      zip: {
    required: true,
    zipcode: true,
    },
      gradyear: "required",
      LocationID: "required",
      CurriculumID: "required",
      areaofstudy: "required",
      LocationID2:"required",
    },
    messages: {
      firstname: "Please enter your First Name",
      lastname: "Please enter your Last Name",
      email: "Please enter a valid email address",
      dayphone: "Please enter a valid phone number",
      address: "Please enter your Address",
      city: "Please enter your City",
      state: "Please enter your State",
      zip: "Please enter your Zip Code",
      gradyear: "Please enter your Graduation Year",
      LocationID: "Please enter your Preferred campus location",
      LocationID2: "Please enter your Preferred campus location",
      CurriculumID: "Please specify the program you are interested in",
      areaofstudy: "Please select your area of study",
    }
  });
});

$(document).ready(function(){
 $(":input").inputmask();
});
