<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<head>  

   	 <!-- Set the viewport width to device width for mobile -->
   	 <meta name="viewport" content="width=device-width, initial-scale=1">

  	<!-- Sass Generated File | Sass file /styles.sass
  	<link href="mobile.css" rel="stylesheet" >-->
  	<!-- Sass Generated File | Sass file /styles.sass -->

  <!--<link href="<?php echo get_bloginfo('template_directory');?>/css/style2.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo('template_directory');?>/css/responsive2.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/animations.css?v=2016.08.29">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.css?v=2016.08.29">-->
 	
	



<?php if(get_the_title() != 'Business Administration' && get_the_title() != 'Interior Design' ): ?>
  <script src="//cdn.optimizely.com/js/7546423263.js" asynch ></script>
<?php endif; ?>  

<?php if(get_the_title() == 'Business Administration' || get_the_title() == 'Interior Design' ): ?>

    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=317342,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    /* DO NOT EDIT BELOW THIS LINE */
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

<?php endif; ?>  


 <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5JDHH2"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5JDHH2');</script>
  <!-- End Google Tag Manager -->

	<?php wp_head(); ?>
</head>
<body class="<?php echo get_the_title();?>" >


