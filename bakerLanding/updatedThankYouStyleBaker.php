<?php
/**
 * Template Name: New Thank You Page Style
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DDiscover your way forward at Baker | Thank you </title>

	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-5QPVDP4':true});</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5JDHH2');</script>
	<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->


	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

	<!-- <link rel="stylesheet" type="text/css" href="dist/css/stylesLandingV1.css"> -->
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JDHH2"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div id="LandingPage">

	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
				<a href="/" class="main-header__logo">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo.svg" alt="" class="main-header__logo-img-red">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo--white.svg" alt="" class="main-header__logo-img-white">
				</a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:810-766-2170"></a>
				<a class="main-header__call-text" href="tel:810-766-2170">810-766-2170</a>
			</div>
		</div>
	</div>
</div>

	<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php echo get_bloginfo('template_directory');?>/assets/images/Discover Landing Page Hero@2x.jpg');">
	<div class="row">
		<div class="LandingPage-fullwidth-video-cta__wrap">
			<h1 class="LandingPage-fullwidth-video-cta__title">Thank you!</h1>
			<div class="LandingPage-fullwidth-video-cta__sub-text">
				<p>We&rsquo;ve sent a confirmation message to the email address you provided. One of our campus representatives should be contacting you shortly.</p>
			</div>
		</div>
	</div>
</div>

<div class="layout-sidebar-right__left-side">
	<div class="text-box">
	<div class="row">
	<p>In the meantime, here are some things that might be helpful to you.</p>
	</div>
	</div>
	<div class="block-grid-links block-grid block-grid--bg-white block-grid--3col block-grid--slim-top-padding">
	<div class="row">
	<div class="block-grid__grid">
	<div class="block-grid__grid-item">
	<a target="_blank" href="https://www.baker.edu/request-info/request-info-thank-you/download-the-new-2017-2018-catalog" class="content-card content-card--hover-effect ">
	<div class="content-card__photo" style="background-image: url('https://www.baker.edu/assets/images/catalog-thumbnail.jpg');"></div>
	<div class="content-card__text-wrap content-card_text-wrap--desktop-gutter">
	<div class="content-card__title">Download the new 2017-2018 catalog</div>
	<div class="content-card__text">
	<p>Learn more about Baker College, our campuses, the academic programs we offer, our admission requirements, and more.</p>
	</div>
	<span href="request-info/request-info-thank-you/download-the-new-2017-2018-catalog" class="read-more content-card__read-more ">Download The Baker College Catalog</span>
	</div>
	</a>
	</div>
	</div>
	</div>
	</div>
</div>



<div class="locations">

<div class="row">
<div class="locations-title">
<h5>For Your Convenience</h5>
<h2> Locations Across Michigan</h2>
</div>

<div class="locations-grid">
	<div class="row">
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Allen Park</h3></div>
			<div class="locations-grid__location-address">4500 Enterprise Drive<br>Allen Park, MI 48101</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Auburn Hills</h3></div>
			<div class="locations-grid__location-address">1500 University Drive<br>Auburn Hills, MI 48326</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Cadillac</h3></div>
			<div class="locations-grid__location-address">9600 E. 13th Street<br>Cadillac, MI 49601</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Clinton Township</h3></div>
			<div class="locations-grid__location-address">34950 Little Mack Avenue<br>Clinton Twp., MI 48035</div>
		</div>
	</div>
	<div class="row">
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Flint</h3></div>
			<div class="locations-grid__location-address">1050 W. Bristol Road<br>Flint, MI 48507</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Jackson</h3></div>
			<div class="locations-grid__location-address">2800 Springport Road<br>Jackson, MI 49202</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Muskegon</h3></div>
			<div class="locations-grid__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
		</div>
		<div class="locations-grid__location">
			<div class="locations-grid__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/MapMarkerShape.svg"><h3>Owosso</h3></div>
			<div class="locations-grid__location-address">1020 S. Washington St.<br>Owosso, MI 48867</div>
		</div>
	</div>
</div>

<div class="locations-callout">
	<div class="row">
		<h2>Global Campuses</h2>
		<div class="locations-callout__location">
			<div class="contain">
			<div class="locations-callout__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online.svg"><h3>Center for Graduate Studies</h3></div>
			<div class="locations-callout__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
		</div>
		</div>
		<div class="locations-callout__location">
			<div class="contain">
			<div class="locations-callout__location-name"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online.svg"><h3>Baker Online</h3></div>
			<div class="locations-callout__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
			</div>
		</div>
	</div>
</div>


<div class="Affiliated_Institutions">
	<div class="row">
		<h2>Affiliated Institutions</h2>
		<div class="Affiliated_Institutions__location">
			<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
			<div class="contain cim">
				<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Muskegon</h4></div>
				<div class="Affiliated_Institutions__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
			</div>
		</div>
		<div class="Affiliated_Institutions__location">
			<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
			<div class="contain cim">
				<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Port Huron</h4></div>
				<div class="Affiliated_Institutions__location-address">3402 Lapper Road<br>Port Huron, MI 48060</div>
			</div>
		</div>
		<div class="Affiliated_Institutions__location">
			<img class="svg" src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/ADI_Logo.svg" />
			<div class="contain adi">
				<div class="Affiliated_Institutions__location-name"><h3>Auto/Diesel Institute of Michigan</h3></div>
				<div class="Affiliated_Institutions__location-name"><h4>Owosso</h4></div>
				<div class="Affiliated_Institutions__location-address">1309 S. M-52<br>Owosso, MI 48867</div>
			</div>
		</div>
	</div>
</div>



</div>
</div>




		<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo--red-white.svg"/>
		</div>
		<p class="footer-section__accredited">Accredited by <span class="footer-section__link_underline"><a href="#">The Higher Learning Commission</a></span>. An  equal opportunity affirmative action institution.
		An approved institution of the <span class="footer-section__link_underline"><a href="#">National Council for State Authorization Reciprocity Agreements (NC-SARA)</a></span>
		and the <span class="footer-section__link_underline"><a href="#">Midwestern Higher Education Compact (MHEC)</a></span>
		</p>
		<div class="footer-section__bottom_rights-reserved"><a href="https://www.baker.edu">Visit baker.edu</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;© 2017 All rights reserved. Baker College</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>

		</body>
</html>
