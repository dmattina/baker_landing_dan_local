<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 */
?>
<?php the_field('extratracking_code'); ?>



<script>
$('input').jvFloat();

$(document).ready(function(){
  $('.Thank .head').css('height', $(window).height() + "px");
  console.log('height added' + $('.Thank .head').height());
  
  

  //$('#SlideInfo h1, .success_holder h2').append('<i> > </i>');
   $('.all_programs > h1').append(' <i>  </i>');

  // All Program in single wysiwig editor - Toggle 
  $('.all_programs > h1').click(function(e){
      var current = $(this).next('blockquote');
      $('.all_programs > blockquote').not(current).hide(100);
      $('.all_programs > h1 i').not($(this).find('i')).removeClass('open_arrow');
      $('.all_programs > h1').not($(this)).removeClass('open'); // open = Mobile  || open_arrow = desktop

      current.slideToggle();
      // open = Mobile  || open_arrow = desktop
      if($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open') ){
          $(this).find('i').removeClass('open_arrow');
          $(this).removeClass('open');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow') || ! $(this).hasClass('open') ) {
          $(this).find('i').addClass('open_arrow');
          $(this).addClass('open')
      }
      if($(window).width() < 767){
                  
      }
  });

  // Separate Program - Toggle Function
  $('.hold > h3').click(function(e){
      var current = $(this).next('section');
      $('.hold > section').not(current).hide(100);
      $('.hold > h3 i').not($(this).find('i')).removeClass('open_arrow');
      current.slideToggle();
      if($(this).find('i').hasClass('open_arrow')){
          $(this).find('i').removeClass('open_arrow');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow')) {
          $(this).find('i').addClass('open_arrow');
      }
  });

  $('.locations_holder .content .title > h3').click(function(e){
    if($(window).width() < 768){
      $('.locations_list, .additional_locations').toggle();
      $(this).find('i').toggleClass('loc_open_arrow');
    }
  })

});


//Dynamic Header height 
    //Adjust element dimensions on resize event
        if($(window).width() > 768){
          $(window).on('resize', function () {
              //$('.head').css('height', $(this).height() + "px");

              var rightSpace = $(window).width() - ($('.content_upper .content').offset().left + $('.content_upper .content').outerWidth());
              var banHeight = $(window).height() - 65 ;
              
          });
        }
        else{
          //$('.head').css('height', $(this).height() + "px");
        }
    

    //Trigger the event
    $(window).trigger('resize');


//Follow form Js
$(window).on('resize', function () {
    
    $('.Thank .head').css('height', $(this).height() + "px");
    formScroll();
});




function formScroll() {
  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
  var fixmePosition = $('.fixme').position();
  var leftmargin = $('.fixme').css('margin-left');
  //var width = document.getElementById('fix').offsetWidth;
  //var intoSize = $('.intro').height() + 35;
  var intoSize = $('#intro').height();
  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
console.log(collapsePoint);

  //$(window).scroll(function() {   
  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
        var currentScroll = $(window).scrollTop();        // get current position  
        var windowSize = $(window).width();
        var formHeight= $('.fixme').height();
        /*var tilesHeight= $('.tiles').offset().top ;
        var stopMe = tilesHeight - formHeight;*/
        var tilesHeight= $('.testimonial_holder').offset().top ;
        var stopMe = tilesHeight - formHeight;

       
        if (currentScroll >= (fixmeTop - 100) && windowSize > 1000) {    // apply position: fixed if you
          
            var fromTop = $('.fixme').offset().top;
            $('.fixme').css({                                 // scroll to that element or below it
          position: 'fixed',
          top: '80px',
          left: fixmePosition.left  + 'px',
          marginTop: '-' + intoSize + 'px',
          });
        } 
        else {                               // apply position: static
        $('.fixme').css({                      // if you scroll above it
          position:'relative',
          top:'auto',
          left :'auto',
            marginTop: '-' + intoSize + 'px',
          });
        }

        /*if (currentScroll >= collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideUp();
        }
        if (currentScroll < collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideDown();
        }*/


        if (currentScroll >= (stopMe - 100) && windowSize > 1000) {           // apply position: fixed if you
              $('.fixme').css({                                 // scroll to that element or below it
                  position: 'absolute',
                  /*width: width + 'px',*/
                  top: stopMe + 'px',
                  left: fixmePosition.left + 'px',
                    marginTop: '-' + intoSize + 'px',
                });
        }
  });
console.log (leftmargin);
}


$(document).ready(function(){
    
    formScroll();
    $('#Intro').click(function(){
      $('form.keypath').slideToggle();
      var formState = $("form.keypath");
      var windowSize = $(window).width();

    })
    $('.footerSplash footer .title a.button').click(function(e){
      if( $(window).width() >= 1001 && $('form.keypath').is(':hidden')){
        e.preventDefault();
        //$('body, html').animate({scrollTop: $("#top").offset().top});
        $('form.keypath').slideDown();
      }
    })
});




function formSwap(){
  var getWidth = $(window).width();
  if(getWidth <= 900){
    $('.mobileForm form.keypathMobile').remove();
    $('div.aside .keypath').detach().insertAfter('.mobileForm #Intro');           
  }
  else
  {
    $('.mobileForm .keypath').detach().insertAfter('div.aside #Intro'); 
  }
}
</script>

<script>
$(function(){
  var mobileTop = $('form.keypath').offset().top;
  var mobileHeight = $('form.keypath').height();
  var windowSize = $(window).width();
  var ShowHeightMobile = mobileTop + mobileHeight
  $(window).scroll(function(){
     var currentScroll = $(window).scrollTop()
      if (currentScroll > ShowHeightMobile && windowSize < 1000 ) {
        //$('.goToFormMobile').css('display', 'block');
        //$('.goToFormMobile').addClass("slideUp");
      } else {
        $('.goToFormMobile').css('display', 'none');
        }
        });
  });
</script>

<script>

/* Mobile Keyboard On Detection for Avoid title text merging with request info form */

// Detect Mobile Device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}; 

$(document).ready(function(){
  
  if(isMobile.any()){

    var _originalSize = $(window).width() + $(window).height();
    var _originalHeadHeight = $(window).height();

      $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
          console.log("keyboard on");
          //alert('keyboard on'); 
          //$(".head").css("height", _originalHeadHeight + "px");  
        }else{
          console.log("keyboard off");
          //alert('keyboard off');
        }
      });
  }

     
  

});
</script>

<?php wp_footer(); ?>

</body>

</html>
