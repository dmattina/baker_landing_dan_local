// form validation
$(".callout-contact__form").validate({
	onfocusout: function(element) {
		this.element(element);
	},
	invalidHandler: function(event, validator) {
	    // 'this' refers to the form
	    var errors = validator.numberOfInvalids();
	    if (errors) {
	      $(".callout-contact__error-message").show();
	    } else {
	      $(".callout-contact__error-message").hide();
	    }
	  }
});

var studentSliderDesktopBuilt = false;
function buildDesktopSlider(){
	if(studentSliderDesktopBuilt) return;
	// Get tallest slider height and set the container to that height
	var desktop = '.callout-featured-student--desktop '
		tallestSlide = ($(desktop + '.callout-featured-student__slide').height()/ parseFloat($("body").css("font-size"))) + 'em';
	$(desktop + '.callout-featured-student__slider').height(tallestSlide);
	$(desktop).addClass('callout-featured-student--initialized');

	var slide1 = desktop + '.callout-featured-student__slide--1 '
		slide2 = desktop + '.callout-featured-student__slide--2 ';

	featuredStudentSlider = new TimelineMax();
	featuredStudentSlider
		.add('slide1')
		.to(slide1 + '.callout-featured-student__quote-panel' , 0.6, {opacity: 0, ease: Power1.easeIn}, 'slide1')
		.to(slide1 + '.callout-featured-student__quote-bg' , 0.6, {css:{width: "100%"}, ease: Power4.easeInOut}, 'slide1')
		.to(slide1 + '.callout-featured-student__photo-panel' , 0.6, {xPercent: 20, ease: Power4.easeInOut}, 'slide1')
		.to('.callout-featured-student__nav-btn--right', 0.6, {yPercent: 100, ease: Power4.easeInOut}, 'slide1')
		.to(slide1 , 0.2, {opacity: 0})
		.add('slide2')
		.from('.callout-featured-student__nav-btn--left', 0.6, {yPercent: 100, ease: Power4.easeInOut}, 'slide2')
		.from(slide2 + '.callout-featured-student__quote-panel' , 0.6, {opacity: 0, ease: Power1.easeIn}, 'slide2+=0.3')
		.from(slide2 + '.callout-featured-student__quote-bg' , 0.6, {css:{width: "100%"}, ease: Power4.easeInOut}, 'slide2')
		.from(slide2 + '.callout-featured-student__photo-panel' , 0.6, {xPercent: -20, ease: Power4.easeInOut}, 'slide2')
		.add('end');
	featuredStudentSlider.pause();

	// Slide to da right
	$('.callout-featured-student__nav-btn--right').on('click',function (e) {
		featuredStudentSlider.play();
	})

	// Slide to da left
	$('.callout-featured-student__nav-btn--left').on('click',function (e) {
		featuredStudentSlider.reverse();
	})

	studentSliderDesktopBuilt = true;
}



function initStudentSlider() {
	var desktopSlider = $('.callout-featured-student').clone()
		.removeClass('callout-featured-student--mobile')
		.addClass('callout-featured-student--desktop');

	$('.callout-featured-student').after(desktopSlider);

	// Create the slick.js mobile slider
	$('.callout-featured-student--mobile .callout-featured-student__slider').slick({
		slide: '.callout-featured-student__slide',
		nextArrow: '<button type="button" class="slick-next"></button>'
	});

	if(screenSize(1)) {
		buildDesktopSlider();
	}
	var resizeTimer;
	$(window).on('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			if(screenSize(1)) {
				buildDesktopSlider();
			}
		}, 250);
	});
}

initStudentSlider();


// form validation

function contactFromValidation() {
	function showFormError(state) {
		if(state) {
			$(".contact-form__error-message").show();
		} else if(!state) {
			$(".contact-form__error-message").hide();
		}
	}

	var contactFromValidator = $(".contact-form__form").validate({
		ignore: [],
		rules: {
		    'zip': {
		      required: true,
		      number: true
		    }
		},
		// Validation on focus out.
		onfocusout: function(element) {
			this.element(element)
			if(this.numberOfInvalids() > 0) {
				showFormError(true);
			} else {
				showFormError(false);
			}
		},
		// Error handler on form submit
		invalidHandler: function(event, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				showFormError(true);
			} else {
				showFormError(false);
			}
		},
		// Needed for styling select2 errors.
		errorPlacement: function (error, element) {
			if (element.hasClass('js-select2')) {
		        error.insertAfter(element.next('span'));  // select2
		    } else {
		        error.insertAfter(element);               // default
		    }
		}
	});

	// Validate jquery ui datepicker fields on select
	$('.contact-form .js-popup-datepicker').on('change', function() {
	    $(this).valid();
	    // this covers the random edge case if a user selects only the date, it will show form error. this prevents the error from showing when date is selected.
	    if(contactFromValidator.numberOfInvalids() == 0) {
	    	showFormError(false);
	    }
	});

	// Validate select2 fields on select
	$('.contact-form .js-select2').on('change', function() {
	    $(this).valid();
	});
}

contactFromValidation();
$('.course-lists .course-lists__program-link').on('click', function(e) {
	e.preventDefault();
	$(this).toggleClass('course-lists__program-link--active');
	$(this).find('.course-lists__list-description').slideToggle();
});

// http://stackoverflow.com/questions/37595170/youtube-embed-api-and-external-js
// in order to control youtube video via api with an external JS script
var yt_int, yt_players={},
initYT = function() {
	$("#fullwidth-video-cta__video-iframe-js").each(function() {
			yt_players[this.id] = new YT.Player(this.id, {
				events: {
					'onReady' : onPlayerReady
				}
			});
	});
};

$.getScript("//www.youtube.com/player_api", function() {
		yt_int = setInterval(function(){
				if(typeof YT === "object"){
						initYT();
						clearInterval(yt_int);
				}
		},500);
});

function onPlayerReady(event){
		// Open video and play it when cta clicked
		$('.fullwidth-video-cta__cta-btn').on('click', function(e) {
			e.preventDefault();
			event.target.playVideo();
			$('.fullwidth-video-cta__modal').fadeIn('slow');
			$('html').addClass('no-scroll');
		});
		$('.fullwidth-video-cta__modal-close').on('click', function(e) {
			e.preventDefault();
			event.target.pauseVideo();
			$('.fullwidth-video-cta__modal').fadeOut('slow');
			$('html').removeClass('no-scroll');
		});
}

// keep the aspect ratio based on window size.
function keepAspectRatio(element) {
	var width,
			height,
			ratio = 16 / 9,
			video = element;

   if ( video ) {
     video.hide();
     width  = $('.fullwidth-video-cta__modal').width() * 0.9;
     height = $('.fullwidth-video-cta__modal').height() - 200;

     if ( height * ratio > width ) {
     	height = width / ratio;
     } else {
			width = height * ratio;
     }
     video.css({
       width  : width,
       height : height
	   }).show();
	}
}

function keepAspectRatioResizeBind(){
	// using this timer to prevent firing too many times.
	var resizeTimer;
	$(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	  	if ($('.fullwidth-video-cta__modal')) {
	  		keepAspectRatio($('.fullwidth-video-cta__modal-content-aspect-ratio-wrap'));
	  	}
	  }, 0);
	});
}

// fire on page load
keepAspectRatio($('.fullwidth-video-cta__modal-content-aspect-ratio-wrap'));
// fire on window resize binding
keepAspectRatioResizeBind();
















var affiliateHeaderClasses = $('.main-header').attr('class');
function toggleAffiliateHeader(state) {
	if(!$('body').hasClass('themed')) return;
	if(state) {
		$('.main-header').attr('class', affiliateHeaderClasses);
	}
	else if(!state) {
		$('.main-header').attr('class', 'main-header');
	}
}

function headerSearch() {
	// Open Search
	$('.main-header__search-btn').on('click', function(e) {
		// Close mega nav if it's open
		if($('.mega-nav').hasClass('mega-nav--active')) {
			closeMegaNav();
		}
		// Remove all possible modifiers so it's the standard header.
		toggleAffiliateHeader(false);
		$('.main-header__search-bar').addClass('main-header__search-bar--active');
		$('.main-header__search-input').focus();
		fadeScreen(true);
	})

	// Close Search
	$('.main-header__search-input-close').on('click', function(e) {
		$('.main-header__search-bar').removeClass('main-header__search-bar--active');
		fadeScreen(false);
		if ($('#screen-size').css('z-index') == 0) {
			toggleAffiliateHeader(true);
		}
	})

	$('.fade-screen').on('click', function(e) {
		$('.main-header__search-bar').removeClass('main-header__search-bar--active');
		toggleAffiliateHeader(true);
		fadeScreen(false);
	})
}

headerSearch();

// Removes the affiliate theme on the header so it's just the baker header. this was requested.
if ($('body').hasClass('themed')) {
	$(".main-header").hover(
		function() {
			if(screenSize(1)){
				toggleAffiliateHeader(false);
			}
		},
		function() {
			// If search is active. Don't remove theme.
			var searchActive = $('.main-header__search-bar').hasClass('main-header__search-bar--active'),
				meganavActive = $('.mega-nav').hasClass('mega-nav--active');
			if(!searchActive && !meganavActive) {
				toggleAffiliateHeader(true);
			}
		}
	);
}



$('.list-filter__toggle').on('click', function(e) {
	e.preventDefault();
	$(this).parent().toggleClass('list-filter--mobile-open');
	$(this).next('.list-filter__form').slideToggle('medium', function(){
		$(this).toggleClass('list-filter__form--open');
		$(this).removeAttr('style');
	});
});

function openMegaNav() {
	$('html').addClass('mega-nav-active');
	$('.main-header__menu-toggle').addClass('main-header__menu-toggle--active');
	fadeScreen(true);
	$('.mega-nav').addClass('mega-nav--active');
	// Show mobile footer links
	$('.main-header__mobile-footer-links').addClass('main-header__mobile-footer-links--active');


	// Highlight the first item in mega nav
	$('.mega-nav__parent-item').first().addClass('hovered');
	// Slidedown the nav
	$('.mega-nav').slideDown('medium');

	toggleAffiliateHeader(false);
}

function closeMegaNav() {
	$('html').removeClass('mega-nav-active');
	$('.main-header__menu-toggle').removeClass('main-header__menu-toggle--active');
	fadeScreen(false);
	$('.mega-nav').removeClass('mega-nav--active');
	// Hide mobile footer links
	$('.main-header__mobile-footer-links').removeClass('main-header__mobile-footer-links--active');

	// Slideup the nav
	$('.mega-nav').slideUp('medium', function() {
		// Wait until nav is done sliding before removing the hovered state.
		$('.mega-nav__parent-item').removeClass('hovered');
	});

	// Turn the affiliate header back on because hover event isn't there for mobile.
	if ($('#screen-size').css('z-index') == 0) {
		toggleAffiliateHeader(true);
	}
}


$('.main-header__menu-toggle').on('click',function (e) {
	var clicked = $(this);
	if(clicked.hasClass('main-header__menu-toggle--active')) {
		closeMegaNav();
	} else {
		openMegaNav();
	}
})

$('.fade-screen').on('click',function (e) {
	closeMegaNav();
})

$('.mega-nav__parent-link').on('click',function (e) {
	e.preventDefault();
	$(this).parent().toggleClass('mega-nav__parent-item--active');
	$(this).next('.mega-nav__popover').slideToggle();
})


$(".mega-nav__parent-list").menuAim({
	activate: activateSubmenu,  // fired on row activation
	deactivate: deactivateSubmenu  // fired on row deactivation
});

function activateSubmenu(row) {
	var $row = $(row);

	$row.addClass('hovered');
}

function deactivateSubmenu(row) {
    var $row = $(row);
    $row.removeClass('hovered');
}



// function megaNavHoverBinding() {
// 	$('.mega-nav__parent-item').hover(function(){
// 		if(!$(this).hasClass('hovered') ) {
// 			$('.mega-nav__parent-item').removeClass('hovered');
// 			$(this).addClass('hovered');
// 		}
// 	})
// }
// megaNavHoverBinding();

// Clone the list-filter into the mobile filter.
if ($('.list-filter').length) {
	var filters = $('.list-filter__form').children().clone();
	$('.mobile-filter__form').append(filters);
}

// Clone sidebar filters into the mobile filter
if ($('.js-mobile-filter').length) {
	$('.js-mobile-filter').each(function(index) {
		var filter = $(this).clone();
		$('.mobile-filter__form').append(filter);
		$('.mobile-filter').addClass('mobile-filter--sidebar-filter');
	});
}


// Open mobile filter
$('.mobile-filter__toggle').on('click',function (e) {
	$(this).next('.mobile-filter__overlay').slideDown();
})

// Cancel button. Close mobile filter
$('.mobile-filter__cancel').on('click',function (e) {
	$('.mobile-filter__overlay').slideUp();
})

// Apply filter button. Close mobile filter
$('.mobile-filter__apply').on('click',function (e) {
	$('.mobile-filter__overlay').slideUp();
})

var Curriculum = jQuery("#LocationID");
var select = $("#LocationID").val();
Curriculum.change(function () {
		var select = $("#LocationID").val();
		if (select == '42975') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582309")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582338")
				document.ppcForm.CurriculumID.options[3] = new Option("Advanced Manufacturing Technology - Associate", "582530")
				document.ppcForm.CurriculumID.options[4] = new Option("Automotive Services Technology - Certificate", "582512")
				document.ppcForm.CurriculumID.options[5] = new Option("Business Administration - Accelerated Program", "582348")
				document.ppcForm.CurriculumID.options[6] = new Option("Business Administration - Associate", "582321")
				document.ppcForm.CurriculumID.options[7] = new Option("CDA Academic Foundations - Certificate", "582679")
				document.ppcForm.CurriculumID.options[8] = new Option("CNC Machinist - Certificate", "582517")
				document.ppcForm.CurriculumID.options[9] = new Option("Computer Programming - Associate", "582393")
				document.ppcForm.CurriculumID.options[10] = new Option("Construction Management - Bachelor", "582563")
				document.ppcForm.CurriculumID.options[11] = new Option("Correctional Studies - Certificate", "582684")
				document.ppcForm.CurriculumID.options[12] = new Option("Criminal Justice - Associate", "582692")
				document.ppcForm.CurriculumID.options[13] = new Option("Criminal Justice - Bachelor", "582726")
				document.ppcForm.CurriculumID.options[14] = new Option("Cyber Defense - Bachelor", "582412")
				document.ppcForm.CurriculumID.options[15] = new Option("Digital Media Design - Associate", "582401")
				document.ppcForm.CurriculumID.options[16] = new Option("Early Childhood Education - Associate", "582701")
				document.ppcForm.CurriculumID.options[17] = new Option("Early Childhood Education - Bachelor", "582735")
				document.ppcForm.CurriculumID.options[18] = new Option("Health Information Technology - Associate", "582588")
				document.ppcForm.CurriculumID.options[19] = new Option("Health Services Administration - Bachelor", "582653")
				document.ppcForm.CurriculumID.options[20] = new Option("Human Services - Associate", "582711")
				document.ppcForm.CurriculumID.options[21] = new Option("Human Services - Bachelor", "582743")
				document.ppcForm.CurriculumID.options[22] = new Option("Information Systems - Bachelor", "582421")
				document.ppcForm.CurriculumID.options[23] = new Option("Information Technology - Associate", "582760")
				document.ppcForm.CurriculumID.options[24] = new Option("Information Technology and Security - Bachelor", "582427")
				document.ppcForm.CurriculumID.options[25] = new Option("Mechanical Technology - Associate", "582554")
				document.ppcForm.CurriculumID.options[26] = new Option("Medical Assistant - Associate", "582598")
				document.ppcForm.CurriculumID.options[27] = new Option("Medical Laboratory Technician - Associate", "582617")
				document.ppcForm.CurriculumID.options[28] = new Option("Nursing - Associate", "582668")
				document.ppcForm.CurriculumID.options[29] = new Option("Occupational Therapy Assistant - Associate", "582618")
				document.ppcForm.CurriculumID.options[30] = new Option("Paralegal - Associate", "582334")
				document.ppcForm.CurriculumID.options[31] = new Option("Pharmacy Technician - Associate", "582623")
				document.ppcForm.CurriculumID.options[32] = new Option("Physical Therapist Assistant - Associate", "582628")
				document.ppcForm.CurriculumID.options[33] = new Option("Surgical Technology - Associate", "582637")
				document.ppcForm.CurriculumID.options[34] = new Option("Therapeutic Massage - Associate", "582643")
				document.ppcForm.CurriculumID.options[35] = new Option("Therapeutic Massage - Certificate", "582578")
		}
		if (select == '42977') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582310")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582339")
				document.ppcForm.CurriculumID.options[3] = new Option("Automotive Services Technology - Associate", "582539")
				document.ppcForm.CurriculumID.options[4] = new Option("Business Administration - Accelerated Program - Bachelor", "582349")
				document.ppcForm.CurriculumID.options[5] = new Option("Business Administration - Associate", "582322")
				document.ppcForm.CurriculumID.options[6] = new Option("Cardiac Sonography - Associate", "582583")
				document.ppcForm.CurriculumID.options[7] = new Option("CDA Academic Foundations - Certificate", "582680")
				document.ppcForm.CurriculumID.options[8] = new Option("Coding Specialist - Associate", "582584")
				document.ppcForm.CurriculumID.options[9] = new Option("Computer Aided Design - Associate", "582545")
				document.ppcForm.CurriculumID.options[10] = new Option("Computer Programming - Associate", "582394")
				document.ppcForm.CurriculumID.options[11] = new Option("Criminal Justice - Associate", "582693")
				document.ppcForm.CurriculumID.options[12] = new Option("Diagnostic Medical Sonography - Associate", "582586")
				document.ppcForm.CurriculumID.options[13] = new Option("Digital Media Design - Associate", "582402")
				document.ppcForm.CurriculumID.options[14] = new Option("Digital Media Design - Bachelor", "582417")
				document.ppcForm.CurriculumID.options[15] = new Option("Early Childhood Education - Associate", "582702")
				document.ppcForm.CurriculumID.options[16] = new Option("Early Childhood Education - Bachelor", "582736")
				document.ppcForm.CurriculumID.options[17] = new Option("Health Services Administration - Bachelor", "582654")
				document.ppcForm.CurriculumID.options[18] = new Option("Human Services - Associate", "582712")
				document.ppcForm.CurriculumID.options[19] = new Option("Human Services - Bachelor", "582744")
				document.ppcForm.CurriculumID.options[20] = new Option("Information Systems - Bachelor", "582422")
				document.ppcForm.CurriculumID.options[21] = new Option("Information Technology - Associate", "582761")
				document.ppcForm.CurriculumID.options[22] = new Option("Information Technology and Security - Bachelor", "582428")
				document.ppcForm.CurriculumID.options[23] = new Option("Interior Design - Associate", "582553")
				document.ppcForm.CurriculumID.options[24] = new Option("Interior Design - Bachelor", "582565")
				document.ppcForm.CurriculumID.options[25] = new Option("Legal Studies - Bachelor", "582368")
				document.ppcForm.CurriculumID.options[26] = new Option("Medical Assistant - Associate", "582599")
				document.ppcForm.CurriculumID.options[27] = new Option("Medical Insurance Specialist - Associate", "582611")
				document.ppcForm.CurriculumID.options[28] = new Option("Nursing (Pre-licensure) - Bachelor", "582669")
				document.ppcForm.CurriculumID.options[29] = new Option("Paralegal - Associate", "582335")
				document.ppcForm.CurriculumID.options[30] = new Option("Phlebotomy Technician - Certificate", "582575")
				document.ppcForm.CurriculumID.options[31] = new Option("Physical Therapist Assistant - Associate", "582629")
				document.ppcForm.CurriculumID.options[32] = new Option("Respiratory Care - Associate", "582636")
				document.ppcForm.CurriculumID.options[33] = new Option("Vascular Ultrasound Technology - Associate", "582648")
				document.ppcForm.CurriculumID.options[34] = new Option("Welding - Associate", "582559")
				document.ppcForm.CurriculumID.options[35] = new Option("Welding - Certificate", "582525")
		}
		if (select == '42978') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582311")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582340")
				document.ppcForm.CurriculumID.options[3] = new Option("Advanced Manufacturing Technology - Associate", "582531")
				document.ppcForm.CurriculumID.options[4] = new Option("Agriculture Technology - Associate", "582534")
				document.ppcForm.CurriculumID.options[5] = new Option("Automotive Services Technology - Associate", "582540")
				document.ppcForm.CurriculumID.options[6] = new Option("Automotive Services Technology - Certificate", "582513")
				document.ppcForm.CurriculumID.options[7] = new Option("Business Administration - Accelerated Program - Bachelor", "582350")
				document.ppcForm.CurriculumID.options[8] = new Option("Business Administration - Associate", "582323")
				document.ppcForm.CurriculumID.options[9] = new Option("CNC Machinist - Certificate", "582518")
				document.ppcForm.CurriculumID.options[10] = new Option("Computer Aided Design - Associate", "582546")
				document.ppcForm.CurriculumID.options[11] = new Option("Correctional Studies - Certificate", "582685")
				document.ppcForm.CurriculumID.options[12] = new Option("Criminal Justice - Associate", "582694")
				document.ppcForm.CurriculumID.options[13] = new Option("Criminal Justice - Bachelor", "582727")
				document.ppcForm.CurriculumID.options[14] = new Option("Digital Media Design - Associate", "582403")
				document.ppcForm.CurriculumID.options[15] = new Option("Early Childhood Education - Associate", "582703")
				document.ppcForm.CurriculumID.options[16] = new Option("Early Childhood Education - Bachelor", "582737")
				document.ppcForm.CurriculumID.options[17] = new Option("Emergency Medical Technician - Basic - Certificate", "582568")
				document.ppcForm.CurriculumID.options[18] = new Option("Emergency Medical Technician - Paramedic - Certificate", "582572")
				document.ppcForm.CurriculumID.options[19] = new Option("Health Services Administration - Bachelor", "582655")
				document.ppcForm.CurriculumID.options[20] = new Option("Human Services - Associate", "582713")
				document.ppcForm.CurriculumID.options[21] = new Option("Human Services - Bachelor", "582745")
				document.ppcForm.CurriculumID.options[22] = new Option("Information Technology - Associate", "582762")
				document.ppcForm.CurriculumID.options[23] = new Option("Information Technology and Security - Bachelor", "582429")
				document.ppcForm.CurriculumID.options[24] = new Option("Mechatronics - Associate", "582557")
				document.ppcForm.CurriculumID.options[25] = new Option("Medical Administrative Specialist - Associate", "582592")
				document.ppcForm.CurriculumID.options[26] = new Option("Medical Assistant - Associate", "582600")
				document.ppcForm.CurriculumID.options[27] = new Option("Medical Insurance Specialist - Associate", "582612")
				document.ppcForm.CurriculumID.options[28] = new Option("Nursing (Pre-licensure) - Bachelor", "582670")
				document.ppcForm.CurriculumID.options[29] = new Option("Surgical Technology - Associate", "582638")
				document.ppcForm.CurriculumID.options[30] = new Option("Therapeutic Massage - Associate", "582644")
				document.ppcForm.CurriculumID.options[31] = new Option("Therapeutic Massage - Certificate", "582579")
				document.ppcForm.CurriculumID.options[32] = new Option("Truck Driving - Certificate", "582523")
				document.ppcForm.CurriculumID.options[33] = new Option("Veterinary Technology - Associate", "582650")
				document.ppcForm.CurriculumID.options[34] = new Option("Welding - Associate", "582560")
				document.ppcForm.CurriculumID.options[35] = new Option("Welding - Certificate", "582526")
		}
		if (select == '42981') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582312")
				document.ppcForm.CurriculumID.options[2] = new Option("Business Administration - Associate", "582324")
				document.ppcForm.CurriculumID.options[3] = new Option("Early Childhood Education - Associate", "582704")
				document.ppcForm.CurriculumID.options[4] = new Option("Human Services - Associate", "582714")
				document.ppcForm.CurriculumID.options[5] = new Option("Human Services - Bachelor", "582746")
				document.ppcForm.CurriculumID.options[6] = new Option("Information Technology - Associate", "582763")
				document.ppcForm.CurriculumID.options[7] = new Option("Medical Assistant - Associate", "582601")
				document.ppcForm.CurriculumID.options[8] = new Option("Pharmacy Technician - Associate", "582624")
				document.ppcForm.CurriculumID.options[9] = new Option("Welding - Certificate", "582527")
		}
		if (select == '42982') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582313")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582341")
				document.ppcForm.CurriculumID.options[3] = new Option("Autobody Technician - Associate", "582536")
				document.ppcForm.CurriculumID.options[4] = new Option("Autobody Technician - Certificate", "582510")
				document.ppcForm.CurriculumID.options[5] = new Option("Automotive Services Technology - Associate", "582541")
				document.ppcForm.CurriculumID.options[6] = new Option("Automotive Services Technology - Certificate", "582514")
				document.ppcForm.CurriculumID.options[7] = new Option("Business Administration - Accelerated Program - Bachelor", "582351")
				document.ppcForm.CurriculumID.options[8] = new Option("Business Administration - Associate", "582325")
				document.ppcForm.CurriculumID.options[9] = new Option("CDA Academic Foundations - Certificate", "582681")
				document.ppcForm.CurriculumID.options[10] = new Option("Computer Programming - Associate", "582395")
				document.ppcForm.CurriculumID.options[11] = new Option("Computer Science - Bachelor", "582408")
				document.ppcForm.CurriculumID.options[12] = new Option("Correctional Studies - Certificate", "582686")
				document.ppcForm.CurriculumID.options[13] = new Option("Criminal Justice - Associate", "582695")
				document.ppcForm.CurriculumID.options[14] = new Option("Criminal Justice - Bachelor", "582728")
				document.ppcForm.CurriculumID.options[15] = new Option("Cyber Defense - Bachelor", "582413")
				document.ppcForm.CurriculumID.options[16] = new Option("Dental Hygiene - Associate", "582585")
				document.ppcForm.CurriculumID.options[17] = new Option("Digital Media Design - Associate", "582404")
				document.ppcForm.CurriculumID.options[18] = new Option("Early Childhood Education - Associate", "582705")
				document.ppcForm.CurriculumID.options[19] = new Option("Early Childhood Education - Bachelor", "582738")
				document.ppcForm.CurriculumID.options[20] = new Option("Early Childhood ZA to ZS - Additional Endorsement - Certificate", "582476")
				document.ppcForm.CurriculumID.options[21] = new Option("Early Childhood ZS (General & Special Education) - Additional Endorsement - Certificate", "582479")
				document.ppcForm.CurriculumID.options[22] = new Option("Elementary Education - Level Change - Certificate", "582464")
				document.ppcForm.CurriculumID.options[23] = new Option("Elementary Education and Early Childhood Education - Level Change - Certificate", "582461")
				document.ppcForm.CurriculumID.options[24] = new Option("Elementary Language Arts - Additional Endorsement - Certificate", "582482")
				document.ppcForm.CurriculumID.options[25] = new Option("Elementary Mathematics - Additional Endorsement - Certificate", "582485")
				document.ppcForm.CurriculumID.options[26] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582489")
				document.ppcForm.CurriculumID.options[27] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582492")
				document.ppcForm.CurriculumID.options[28] = new Option("Elementary Teacher Preparation Early Childhood ZS (General & Special Education) - Bachelor", "582440")
				document.ppcForm.CurriculumID.options[29] = new Option("Elementary Teacher Preparation Language Arts - Bachelor", "582443")
				document.ppcForm.CurriculumID.options[30] = new Option("Elementary Teacher Preparation Mathematics - Bachelor", "582446")
				document.ppcForm.CurriculumID.options[31] = new Option("Elementary Teacher Preparation Social Studies - Bachelor", "582449")
				document.ppcForm.CurriculumID.options[32] = new Option("Emergency Medical Technician - Basic - Certificate", "582569")
				document.ppcForm.CurriculumID.options[33] = new Option("Emergency Medical Technician - Paramedic - Certificate", "582573")
				document.ppcForm.CurriculumID.options[34] = new Option("Game Software Development - Bachelor", "582419")
				document.ppcForm.CurriculumID.options[35] = new Option("Health Information Technology - Associate", "582589")
				document.ppcForm.CurriculumID.options[36] = new Option("Health Services Administration - Bachelor", "582656")
				document.ppcForm.CurriculumID.options[37] = new Option("Heating, Ventilation, Air Conditioning Technology - Certificate", "582522")
				document.ppcForm.CurriculumID.options[38] = new Option("Human Services - Associate", "582715")
				document.ppcForm.CurriculumID.options[39] = new Option("Human Services - Bachelor", "582747")
				document.ppcForm.CurriculumID.options[40] = new Option("Information Systems - Bachelor", "582423")
				document.ppcForm.CurriculumID.options[41] = new Option("Information Technology - Associate", "582764")
				document.ppcForm.CurriculumID.options[42] = new Option("Information Technology and Security - Bachelor", "582430")
				document.ppcForm.CurriculumID.options[43] = new Option("Law Enforcement Academy (Police) - Associate", "582723")
				document.ppcForm.CurriculumID.options[44] = new Option("Law Enforcement Academy (Polilce) - Bachelor", "582753")
				document.ppcForm.CurriculumID.options[45] = new Option("Medical Administrative Specialist - Associate", "582593")
				document.ppcForm.CurriculumID.options[46] = new Option("Medical Assistant - Associate", "582602")
				document.ppcForm.CurriculumID.options[47] = new Option("Nursing (Pre-licensure) - Bachelor", "582671")
				document.ppcForm.CurriculumID.options[48] = new Option("Pharmacy Technician - Associate", "582625")
				document.ppcForm.CurriculumID.options[49] = new Option("Radiologic Technology - Associate", "582633")
				document.ppcForm.CurriculumID.options[50] = new Option("Secondary English - Additional Endorsement - Certificate", "582495")
				document.ppcForm.CurriculumID.options[51] = new Option("Secondary English - Level Change - Certificate", "582467")
				document.ppcForm.CurriculumID.options[52] = new Option("Secondary Mathematics - Additional Endorsement - Certificate", "582501")
				document.ppcForm.CurriculumID.options[53] = new Option("Secondary Mathematics - Level Change - Certificate", "582470")
				document.ppcForm.CurriculumID.options[54] = new Option("Secondary Social Studies - Additional Endorsement - Certificate", "582505")
				document.ppcForm.CurriculumID.options[55] = new Option("Secondary Social Studies - Level Change - Certificate", "582473")
				document.ppcForm.CurriculumID.options[56] = new Option("Secondary Teacher Preparation English and Mathematics - Double Major - Bachelor", "582452")
				document.ppcForm.CurriculumID.options[57] = new Option("Secondary Teacher Preparation English and Social Studies - Double Major - Bachelor", "582455")
				document.ppcForm.CurriculumID.options[58] = new Option("Secondary Teacher Preparation Mathematics and Social Studies - Double Major - Bachelor", "582458")
				document.ppcForm.CurriculumID.options[59] = new Option("Surgical Technology - Associate", "582639")
				document.ppcForm.CurriculumID.options[60] = new Option("Therapeutic Massage - Associate", "582645")
				document.ppcForm.CurriculumID.options[61] = new Option("Therapeutic Massage - Certificate", "582580")
				document.ppcForm.CurriculumID.options[62] = new Option("Veterinary Technology - Associate", "582651")
		}
		if (select == '42983') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582314")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582342")
				document.ppcForm.CurriculumID.options[3] = new Option("Early Childhood Education - Associate", "582706")
				document.ppcForm.CurriculumID.options[4] = new Option("Information Technology - Associate", "582765")
				document.ppcForm.CurriculumID.options[5] = new Option("Medical Assistant - Associate", "582603")
				document.ppcForm.CurriculumID.options[6] = new Option("Medical Insurance Specialist - Associate", "582613")
		}
		if (select == '42984') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582315")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582343")
				document.ppcForm.CurriculumID.options[3] = new Option("Advanced Manufacturing Technology - Associate", "582532")
				document.ppcForm.CurriculumID.options[4] = new Option("Autobody Technician - Associate", "582537")
				document.ppcForm.CurriculumID.options[5] = new Option("Autobody Technician - Certificate", "582511")
				document.ppcForm.CurriculumID.options[6] = new Option("Automotive Restoration Technology - Associate", "582538")
				document.ppcForm.CurriculumID.options[7] = new Option("Automotive Services Technology - Associate", "582542")
				document.ppcForm.CurriculumID.options[8] = new Option("Automotive Services Technology - Certificate", "582515")
				document.ppcForm.CurriculumID.options[9] = new Option("Business Administration - Accelerated Program", "582352")
				document.ppcForm.CurriculumID.options[10] = new Option("Business Administration - Associate", "582326")
				document.ppcForm.CurriculumID.options[11] = new Option("CDA Academic Foundations - Certificate", "582682")
				document.ppcForm.CurriculumID.options[12] = new Option("Civil Engineering - Bachelor", "582562")
				document.ppcForm.CurriculumID.options[13] = new Option("CNC Machinist - Certificate", "582519")
				document.ppcForm.CurriculumID.options[14] = new Option("Computer Aided Design - Associate", "582547")
				document.ppcForm.CurriculumID.options[15] = new Option("Computer Programming - Associate", "582396")
				document.ppcForm.CurriculumID.options[16] = new Option("Computer Science - Bachelor", "582409")
				document.ppcForm.CurriculumID.options[17] = new Option("Correctional Studies - Certificate", "582687")
				document.ppcForm.CurriculumID.options[18] = new Option("Criminal Justice - Associate", "582696")
				document.ppcForm.CurriculumID.options[19] = new Option("Criminal Justice - Bachelor", "582729")
				document.ppcForm.CurriculumID.options[20] = new Option("Cyber Defense - Bachelor", "582414")
				document.ppcForm.CurriculumID.options[21] = new Option("Digital Media Design - Associate", "582405")
				document.ppcForm.CurriculumID.options[22] = new Option("Early Childhood Education - Associate", "582707")
				document.ppcForm.CurriculumID.options[23] = new Option("Early Childhood Education - Bachelor", "582739")
				document.ppcForm.CurriculumID.options[24] = new Option("Electrical Engineering - Bachelor", "582564")
				document.ppcForm.CurriculumID.options[25] = new Option("Health Information Technology - Associate", "582590")
				document.ppcForm.CurriculumID.options[26] = new Option("Health Services Administration - Bachelor", "582657")
				document.ppcForm.CurriculumID.options[27] = new Option("Human Services - Associate", "582716")
				document.ppcForm.CurriculumID.options[28] = new Option("Human Services - Bachelor", "582748")
				document.ppcForm.CurriculumID.options[29] = new Option("Information Technology - Associate", "582766")
				document.ppcForm.CurriculumID.options[30] = new Option("Information Technology and Security - Bachelor", "582431")
				document.ppcForm.CurriculumID.options[31] = new Option("Interpreter Training - Bachelor", "583469")
				document.ppcForm.CurriculumID.options[32] = new Option("Law Enforcement Academy (Police) - Associate", "582724")
				document.ppcForm.CurriculumID.options[33] = new Option("Law Enforcement Academy (Polilce) - Bachelor", "582754")
				document.ppcForm.CurriculumID.options[34] = new Option("Mechanical Engineering - Bachelor", "582566")
				document.ppcForm.CurriculumID.options[35] = new Option("Mechanical Technology - Associate", "582555")
				document.ppcForm.CurriculumID.options[36] = new Option("Medical Administrative Specialist - Associate", "582594")
				document.ppcForm.CurriculumID.options[37] = new Option("Medical Assistant - Associate", "582604")
				document.ppcForm.CurriculumID.options[38] = new Option("Medical Insurance Specialist - Associate", "582614")
				document.ppcForm.CurriculumID.options[39] = new Option("Nursing (Pre-licensure) - Bachelor", "582672")
				document.ppcForm.CurriculumID.options[40] = new Option("Occupational Therapy - MOT (Master's)", "582666")
				document.ppcForm.CurriculumID.options[41] = new Option("Orthotic/Prosthetic Technology - Associate", "582622")
				document.ppcForm.CurriculumID.options[42] = new Option("Pharmacy Technician - Associate", "582626")
				document.ppcForm.CurriculumID.options[43] = new Option("Photonics and Laser Technology - Associate", "582558")
				document.ppcForm.CurriculumID.options[44] = new Option("Physical Therapist Assistant - Associate", "582630")
				document.ppcForm.CurriculumID.options[45] = new Option("Polysomnographic Technology - Associate", "582632")
				document.ppcForm.CurriculumID.options[46] = new Option("Pre-Occupational Therapy - Bachelor", "582664")
				document.ppcForm.CurriculumID.options[47] = new Option("Special Needs Paraprofesisonal/Behavioral Technician - Certificate", "582690")
				document.ppcForm.CurriculumID.options[48] = new Option("Sterile Processing Technician - Certificate", "582576")
				document.ppcForm.CurriculumID.options[49] = new Option("Surgical Technology - Associate", "582640")
				document.ppcForm.CurriculumID.options[50] = new Option("Truck Driving - Certificate", "582524")
				document.ppcForm.CurriculumID.options[51] = new Option("Veterinary Technology - Associate", "582652")
				document.ppcForm.CurriculumID.options[52] = new Option("Welding - Associate", "582561")
				document.ppcForm.CurriculumID.options[53] = new Option("Welding - Certificate", "582528")
		}
		if (select == '42985') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582316")
				document.ppcForm.CurriculumID.options[2] = new Option("Business Administration - Accelerated Program - Bachelor", "582353")
				document.ppcForm.CurriculumID.options[3] = new Option("Business Administration - Associate", "582327")
				document.ppcForm.CurriculumID.options[4] = new Option("Emergency Medical Technician - Basic - Certificate", "582570")
				document.ppcForm.CurriculumID.options[5] = new Option("Health Services Administration - Bachelor", "582658")
				document.ppcForm.CurriculumID.options[6] = new Option("Medical Assistant - Associate", "582605")
		}
		if (select == '42986') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582317")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582344")
				document.ppcForm.CurriculumID.options[3] = new Option("Advanced Manufacturing Technology - Associate", "582533")
				document.ppcForm.CurriculumID.options[4] = new Option("Business Administration - Accelerated Program - Bachelor", "582354")
				document.ppcForm.CurriculumID.options[5] = new Option("Business Administration - Associate", "582328")
				document.ppcForm.CurriculumID.options[6] = new Option("CDA Academic Foundations - Certificate", "582683")
				document.ppcForm.CurriculumID.options[7] = new Option("CNC Machinist - Certificate", "582520")
				document.ppcForm.CurriculumID.options[8] = new Option("Computer Aided Design - Associate", "582548")
				document.ppcForm.CurriculumID.options[9] = new Option("Computer Programming - Associate", "582397")
				document.ppcForm.CurriculumID.options[10] = new Option("Correctional Studies - Certificate", "582688")
				document.ppcForm.CurriculumID.options[11] = new Option("Criminal Justice - Associate", "582697")
				document.ppcForm.CurriculumID.options[12] = new Option("Criminal Justice - Bachelor", "582730")
				document.ppcForm.CurriculumID.options[13] = new Option("Cyber Defense - Bachelor", "582415")
				document.ppcForm.CurriculumID.options[14] = new Option("Early Childhood Education - Associate", "582708")
				document.ppcForm.CurriculumID.options[15] = new Option("Early Childhood Education - Bachelor", "582740")
				document.ppcForm.CurriculumID.options[16] = new Option("Health Information Technology - Associate", "582591")
				document.ppcForm.CurriculumID.options[17] = new Option("Health Services Administration - Bachelor", "582659")
				document.ppcForm.CurriculumID.options[18] = new Option("Human Services - Associate", "582717")
				document.ppcForm.CurriculumID.options[19] = new Option("Human Services - Bachelor", "582749")
				document.ppcForm.CurriculumID.options[20] = new Option("Information Systems - Bachelor", "582424")
				document.ppcForm.CurriculumID.options[21] = new Option("Information Technology - Associate", "582767")
				document.ppcForm.CurriculumID.options[22] = new Option("Information Technology and Security - Bachelor", "582432")
				document.ppcForm.CurriculumID.options[23] = new Option("Mechanical Engineering - Bachelor", "582567")
				document.ppcForm.CurriculumID.options[24] = new Option("Mechanical Technology - Associate", "582556")
				document.ppcForm.CurriculumID.options[25] = new Option("Medical Administrative Specialist - Associate", "582595")
				document.ppcForm.CurriculumID.options[26] = new Option("Medical Assistant - Associate", "582606")
				document.ppcForm.CurriculumID.options[27] = new Option("Medical Insurance Specialist - Associate", "582615")
				document.ppcForm.CurriculumID.options[28] = new Option("Nursing (Pre-licensure) - Bachelor", "582673")
				document.ppcForm.CurriculumID.options[29] = new Option("Paralegal - Associate", "582336")
				document.ppcForm.CurriculumID.options[30] = new Option("Practical Nurse - Certificate", "582667")
				document.ppcForm.CurriculumID.options[31] = new Option("Radiation Therapy - Bachelor", "582665")
				document.ppcForm.CurriculumID.options[32] = new Option("Special Needs Paraprofesisonal/Behavioral Technician - Certificate", "582691")
				document.ppcForm.CurriculumID.options[33] = new Option("Surgical Technology - Associate", "582641")
				document.ppcForm.CurriculumID.options[34] = new Option("Therapeutic Massage - Associate", "582646")
				document.ppcForm.CurriculumID.options[35] = new Option("Therapeutic Massage - Certificate", "582581")
		}
		if (select == '42987') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582318")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582345")
				document.ppcForm.CurriculumID.options[3] = new Option("Business Administration - Accelerated Program - Bachelor", "582355")
				document.ppcForm.CurriculumID.options[4] = new Option("Business Administration - Associate", "582329")
				document.ppcForm.CurriculumID.options[5] = new Option("Computer Aided Design - Associate", "582549")
				document.ppcForm.CurriculumID.options[6] = new Option("Computer Programming - Associate", "582398")
				document.ppcForm.CurriculumID.options[7] = new Option("Computer Science - Bachelor", "582410")
				document.ppcForm.CurriculumID.options[8] = new Option("Correctional Studies - Certificate", "582689")
				document.ppcForm.CurriculumID.options[9] = new Option("Criminal Justice - Associate", "582698")
				document.ppcForm.CurriculumID.options[10] = new Option("Criminal Justice - Bachelor", "582731")
				document.ppcForm.CurriculumID.options[11] = new Option("Digital Media Design - Associate", "582406")
				document.ppcForm.CurriculumID.options[12] = new Option("Digital Media Design - Bachelor", "582418")
				document.ppcForm.CurriculumID.options[13] = new Option("Early Childhood Education - Associate", "582709")
				document.ppcForm.CurriculumID.options[14] = new Option("Early Childhood Education - Bachelor", "582741")
				document.ppcForm.CurriculumID.options[15] = new Option("Early Childhood ZA to ZS - Additional Endorsement - Certificate", "582477")
				document.ppcForm.CurriculumID.options[16] = new Option("Early Childhood ZS (General & Special Education) - Additional Endorsement - Certificate", "582480")
				document.ppcForm.CurriculumID.options[17] = new Option("Elementary Education - Level Change - Certificate", "582465")
				document.ppcForm.CurriculumID.options[18] = new Option("Elementary Education and Early Childhood Education - Level Change - Certificate", "582462")
				document.ppcForm.CurriculumID.options[19] = new Option("Elementary Language Arts - Additional Endorsement - Certificate", "582483")
				document.ppcForm.CurriculumID.options[20] = new Option("Elementary Mathematics - Additional Endorsement - Certificate", "582486")
				document.ppcForm.CurriculumID.options[21] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582490")
				document.ppcForm.CurriculumID.options[22] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582493")
				document.ppcForm.CurriculumID.options[23] = new Option("Elementary Teacher Preparation Early Childhood ZS (General & Special Education) - Bachelor", "582441")
				document.ppcForm.CurriculumID.options[24] = new Option("Elementary Teacher Preparation Language Arts - Bachelor", "582444")
				document.ppcForm.CurriculumID.options[25] = new Option("Elementary Teacher Preparation Mathematics - Bachelor", "582447")
				document.ppcForm.CurriculumID.options[26] = new Option("Elementary Teacher Preparation Social Studies - Bachelor", "582450")
				document.ppcForm.CurriculumID.options[27] = new Option("Emergency Medical Technician - Basic - Certificate", "582571")
				document.ppcForm.CurriculumID.options[28] = new Option("Emergency Medical Technician - Paramedic - Certificate", "582574")
				document.ppcForm.CurriculumID.options[29] = new Option("Health Services Administration - Bachelor", "582660")
				document.ppcForm.CurriculumID.options[30] = new Option("Human Resource Management - Bachelor", "582365")
				document.ppcForm.CurriculumID.options[31] = new Option("Human Services - Associate", "582718")
				document.ppcForm.CurriculumID.options[32] = new Option("Human Services - Bachelor", "582750")
				document.ppcForm.CurriculumID.options[33] = new Option("Information Systems - Bachelor", "582425")
				document.ppcForm.CurriculumID.options[34] = new Option("Information Technology - Associate", "582768")
				document.ppcForm.CurriculumID.options[35] = new Option("Information Technology and Security - Bachelor", "582433")
				document.ppcForm.CurriculumID.options[36] = new Option("Medical Administrative Specialist - Associate", "582596")
				document.ppcForm.CurriculumID.options[37] = new Option("Medical Assistant - Associate", "582607")
				document.ppcForm.CurriculumID.options[38] = new Option("Medical Insurance Specialist - Associate", "582616")
				document.ppcForm.CurriculumID.options[39] = new Option("Nursing (Pre-licensure) - Bachelor", "582674")
				document.ppcForm.CurriculumID.options[40] = new Option("Occupational Therapy Assistant - Associate", "582619")
				document.ppcForm.CurriculumID.options[41] = new Option("Paralegal - Associate", "582337")
				document.ppcForm.CurriculumID.options[42] = new Option("Pharmacy Technician - Associate", "582627")
				document.ppcForm.CurriculumID.options[43] = new Option("Physical Therapist Assistant - Associate", "582631")
				document.ppcForm.CurriculumID.options[44] = new Option("Radiologic Technology - Associate", "582634")
				document.ppcForm.CurriculumID.options[45] = new Option("Secondary English - Additional Endorsement - Certificate", "582496")
				document.ppcForm.CurriculumID.options[46] = new Option("Secondary English - Level Change - Certificate", "582468")
				document.ppcForm.CurriculumID.options[47] = new Option("Secondary Mathematics - Additional Endorsement - Certificate", "582502")
				document.ppcForm.CurriculumID.options[48] = new Option("Secondary Mathematics - Level Change - Certificate", "582471")
				document.ppcForm.CurriculumID.options[49] = new Option("Secondary Social Studies - Additional Endorsement - Certificate", "582506")
				document.ppcForm.CurriculumID.options[50] = new Option("Secondary Social Studies - Level Change - Certificate", "582474")
				document.ppcForm.CurriculumID.options[51] = new Option("Secondary Teacher Preparation English and Mathematics - Double Major - Bachelor", "582453")
				document.ppcForm.CurriculumID.options[52] = new Option("Secondary Teacher Preparation English and Social Studies - Double Major - Bachelor", "582456")
				document.ppcForm.CurriculumID.options[53] = new Option("Secondary Teacher Preparation Mathematics and Social Studies - Double Major - Bachelor", "582459")
				document.ppcForm.CurriculumID.options[54] = new Option("Sterile Processing Technician - Certificate", "582577")
				document.ppcForm.CurriculumID.options[55] = new Option("Surgical Technology - Associate", "582642")
				document.ppcForm.CurriculumID.options[56] = new Option("Therapeutic Massage - Associate", "582647")
				document.ppcForm.CurriculumID.options[57] = new Option("Therapeutic Massage - Certificate", "582582")
		}
		if (select == '42988') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582319")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582346")
				document.ppcForm.CurriculumID.options[3] = new Option("Automotive Services Technology - Associate", "582543")
				document.ppcForm.CurriculumID.options[4] = new Option("Automotive Services Technology - Certificate", "582516")
				document.ppcForm.CurriculumID.options[5] = new Option("Automotive Services Technology - MOPAR CAP - Associate", "582544")
				document.ppcForm.CurriculumID.options[6] = new Option("Business Administration - Accelerated Program - Bachelor", "582356")
				document.ppcForm.CurriculumID.options[7] = new Option("Business Administration - Associate", "582330")
				document.ppcForm.CurriculumID.options[8] = new Option("Computer Programming - Associate", "582399")
				document.ppcForm.CurriculumID.options[9] = new Option("Computer Science - Bachelor", "582411")
				document.ppcForm.CurriculumID.options[10] = new Option("Criminal Justice - Associate", "582699")
				document.ppcForm.CurriculumID.options[11] = new Option("Criminal Justice - Bachelor", "582732")
				document.ppcForm.CurriculumID.options[12] = new Option("Diagnostic Medical Sonography - Associate", "582587")
				document.ppcForm.CurriculumID.options[13] = new Option("Diesel Service Technology - Associate", "582550")
				document.ppcForm.CurriculumID.options[14] = new Option("Diesel Service Technology - Certificate", "582521")
				document.ppcForm.CurriculumID.options[15] = new Option("Early Childhood Education - Associate", "582710")
				document.ppcForm.CurriculumID.options[16] = new Option("Early Childhood Education - Bachelor", "582742")
				document.ppcForm.CurriculumID.options[17] = new Option("Early Childhood ZA to ZS - Additional Endorsement - Certificate", "582478")
				document.ppcForm.CurriculumID.options[18] = new Option("Early Childhood ZS (General & Special Education) - Additional Endorsement - Certificate", "582481")
				document.ppcForm.CurriculumID.options[19] = new Option("Elementary Education - Level Change - Certificate", "582466")
				document.ppcForm.CurriculumID.options[20] = new Option("Elementary Education and Early Childhood Education - Level Change - Certificate", "582463")
				document.ppcForm.CurriculumID.options[21] = new Option("Elementary Language Arts - Additional Endorsement - Certificate", "582484")
				document.ppcForm.CurriculumID.options[22] = new Option("Elementary Mathematics - Additional Endorsement - Certificate", "582487")
				document.ppcForm.CurriculumID.options[23] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582491")
				document.ppcForm.CurriculumID.options[24] = new Option("Elementary Social Studies - Additional Endorsement - Certificate", "582494")
				document.ppcForm.CurriculumID.options[25] = new Option("Elementary Teacher Preparation Early Childhood ZS (General & Special Education) - Bachelor", "582442")
				document.ppcForm.CurriculumID.options[26] = new Option("Elementary Teacher Preparation Language Arts - Bachelor", "582445")
				document.ppcForm.CurriculumID.options[27] = new Option("Elementary Teacher Preparation Mathematics - Bachelor", "582448")
				document.ppcForm.CurriculumID.options[28] = new Option("Elementary Teacher Preparation Social Studies - Bachelor", "582451")
				document.ppcForm.CurriculumID.options[29] = new Option("Health Services Administration - Bachelor", "582661")
				document.ppcForm.CurriculumID.options[30] = new Option("Human Services - Associate", "582719")
				document.ppcForm.CurriculumID.options[31] = new Option("Human Services - Bachelor", "582751")
				document.ppcForm.CurriculumID.options[32] = new Option("Information Technology - Associate", "582769")
				document.ppcForm.CurriculumID.options[33] = new Option("Information Technology and Security - Bachelor", "582434")
				document.ppcForm.CurriculumID.options[34] = new Option("Law Enforcement Academy (Police) - Associate", "582725")
				document.ppcForm.CurriculumID.options[35] = new Option("Law Enforcement Academy (Polilce) - Bachelor", "582755")
				document.ppcForm.CurriculumID.options[36] = new Option("Medical Assistant - Associate", "582608")
				document.ppcForm.CurriculumID.options[37] = new Option("Nursing (Pre-licensure) - Bachelor", "582675")
				document.ppcForm.CurriculumID.options[38] = new Option("Occupational Therapy Assistant - Associate", "582620")
				document.ppcForm.CurriculumID.options[39] = new Option("Radiologic Technology - Associate", "582635")
				document.ppcForm.CurriculumID.options[40] = new Option("Secondary English - Additional Endorsement - Certificate", "582497")
				document.ppcForm.CurriculumID.options[41] = new Option("Secondary English - Level Change - Certificate", "582469")
				document.ppcForm.CurriculumID.options[42] = new Option("Secondary Mathematics - Additional Endorsement - Certificate", "582503")
				document.ppcForm.CurriculumID.options[43] = new Option("Secondary Mathematics - Level Change - Certificate", "582472")
				document.ppcForm.CurriculumID.options[44] = new Option("Secondary Social Studies - Additional Endorsement - Certificate", "582507")
				document.ppcForm.CurriculumID.options[45] = new Option("Secondary Social Studies - Level Change - Certificate", "582475")
				document.ppcForm.CurriculumID.options[46] = new Option("Secondary Teacher Preparation English and Mathematics - Double Major - Bachelor", "582454")
				document.ppcForm.CurriculumID.options[47] = new Option("Secondary Teacher Preparation English and Social Studies - Double Major - Bachelor", "582457")
				document.ppcForm.CurriculumID.options[48] = new Option("Secondary Teacher Preparation Mathematics and Social Studies - Double Major - Bachelor", "582460")
				document.ppcForm.CurriculumID.options[49] = new Option("Vascular Ultrasound Technology - Associate", "582649")
				document.ppcForm.CurriculumID.options[50] = new Option("Welding - Certificate", "582529")
		}
		if (select == '42989') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Business Administration - Accelerated Program", "582357")
				document.ppcForm.CurriculumID.options[2] = new Option("Business Administration - Associate", "582331")
				document.ppcForm.CurriculumID.options[3] = new Option("Criminal Justice - Associate", "582700")
				document.ppcForm.CurriculumID.options[4] = new Option("Criminal Justice - Bachelor", "582733")
				document.ppcForm.CurriculumID.options[5] = new Option("Health Services Administration", "582662")
				document.ppcForm.CurriculumID.options[6] = new Option("Human Services - Associate", "582720")
				document.ppcForm.CurriculumID.options[7] = new Option("Human Services - Bachelor", "582752")
				document.ppcForm.CurriculumID.options[8] = new Option("Information Technology - Associate", "582770")
				document.ppcForm.CurriculumID.options[9] = new Option("Information Technology and Security - Bachelor", "582435")
				document.ppcForm.CurriculumID.options[10] = new Option("Medical Administrative Specialist - Associate", "582597")
				document.ppcForm.CurriculumID.options[11] = new Option("Medical Assistant - Associate", "582609")
		}
		if (select == '42990') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Business Administration - Associate", "582332")
				document.ppcForm.CurriculumID.options[2] = new Option("Human Services - Associate", "582721")
				document.ppcForm.CurriculumID.options[3] = new Option("Medical Assistant - Associate", "582610")
		}
		if (select == '1286') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - Associate", "582320")
				document.ppcForm.CurriculumID.options[2] = new Option("Accounting - Bachelor", "582347")
				document.ppcForm.CurriculumID.options[3] = new Option("Business Administration - Accelerated Program - Bachelor", "582358")
				document.ppcForm.CurriculumID.options[4] = new Option("Business Administration - Associate", "582333")
				document.ppcForm.CurriculumID.options[5] = new Option("Computer Programming - Associate", "582400")
				document.ppcForm.CurriculumID.options[6] = new Option("Computer Science - Bachelor", "7396")
				document.ppcForm.CurriculumID.options[7] = new Option("Criminal Justice - Bachelor", "582734")
				document.ppcForm.CurriculumID.options[8] = new Option("Database Technology - Bachelor", "582416")
				document.ppcForm.CurriculumID.options[9] = new Option("Elementary Mathematics - Additional Endorsement - Certificate", "582488")
				document.ppcForm.CurriculumID.options[10] = new Option("Finance - Bachelor", "582407")
				document.ppcForm.CurriculumID.options[11] = new Option("Game Software Development - Bachelor", "582420")
				document.ppcForm.CurriculumID.options[12] = new Option("General Studies - Bachelor", "582757")
				document.ppcForm.CurriculumID.options[13] = new Option("Health Services Administration - Bachelor", "582663")
				document.ppcForm.CurriculumID.options[14] = new Option("Information Systems - Bachelor", "582426")
				document.ppcForm.CurriculumID.options[15] = new Option("Information Technology - Associate", "582771")
				document.ppcForm.CurriculumID.options[16] = new Option("Information Technology and Security - Bachelor", "582436")
				document.ppcForm.CurriculumID.options[17] = new Option("Law Enforcement Academy (Police) - Bachelor", "582756")
				document.ppcForm.CurriculumID.options[18] = new Option("Mobile Application Software Engineering - Bachelor", "582437")
				document.ppcForm.CurriculumID.options[19] = new Option("Nursing (Post-licensure) - Bachelor", "582676")
				document.ppcForm.CurriculumID.options[20] = new Option("Psychology - Bachelor", "582758")
				document.ppcForm.CurriculumID.options[21] = new Option("Secondary Mathematics - Additional Endorsement - Certificate", "582504")
				document.ppcForm.CurriculumID.options[22] = new Option("Supply Chain Management - Bachelor", "582389")
		}
		if (select == '43095') {
				document.ppcForm.CurriculumID.options.length = 0
				document.ppcForm.CurriculumID.options[0] = new Option("Select A Program", "", "true", "true")
				document.ppcForm.CurriculumID.options[1] = new Option("Accounting - MBA (Master's)", "583087")
				document.ppcForm.CurriculumID.options[2] = new Option("Business Administration - DBA (Doctorate)", "583100")
				document.ppcForm.CurriculumID.options[3] = new Option("Business Intelligence - MBA (Master's)", "583088")
				document.ppcForm.CurriculumID.options[4] = new Option("Business Intelligence - MSIS (Master's)", "583089")
				document.ppcForm.CurriculumID.options[5] = new Option("Educational Leadership: Higher Education - MSEE (Master's)", "583377")
				document.ppcForm.CurriculumID.options[6] = new Option("Educational Leadership: K-12 - MSEE (Master's)", "583090")
				document.ppcForm.CurriculumID.options[7] = new Option("Finance - MBA (Master's)", "583091")
				document.ppcForm.CurriculumID.options[8] = new Option("Healthcare Management - MBA (Master's)", "583092")
				document.ppcForm.CurriculumID.options[9] = new Option("Human Resource Management - MBA", "583093")
				document.ppcForm.CurriculumID.options[10] = new Option("Information Systems - MBA (Master's)", "583094")
				document.ppcForm.CurriculumID.options[11] = new Option("Information Systems - MSIS (Master's)", "583095")
				document.ppcForm.CurriculumID.options[12] = new Option("Leadership Studies - MBA (Master's)", "583096")
				document.ppcForm.CurriculumID.options[13] = new Option("Marketing - MBA", "583097")
				document.ppcForm.CurriculumID.options[14] = new Option("Nursing Administration - Master's", "583098")
				document.ppcForm.CurriculumID.options[15] = new Option("Nursing Education - Master's", "583099")
		}
		$('#CurriculumID').children().each(function(){
				$(this).addClass('dropdownvalue');
		});
});

$.validator.addMethod('customphone', function (value, element) {
		return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
}, "Please enter a valid phone number");
$.validator.addMethod("fillProgram", function (value, element, param) {
		return this.optional(element) || value != param;
}, "Please specify the program you are intersted in");
$.validator.addMethod("zipcode", function (value, element) {
		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");
$.validator.addMethod("nonNumeric", function (value, element) {
		return this.optional(element) || !value.match(/[0-9]+/);
}, "Only alphabatic characters allowed.");
jQuery().ready(function () {
		// validate signup form on keyup and submit
		$(".contact-form.keypath").validate({
				rules: {
						firstname: "required",
						lastname: "required",
						email: {
								required: true,
								email: true
						},
						dayphone: {
								required: true,
								customphone: true
						},
						address: "required",
						city: {
								required: true,
								nonNumeric: true
						},
						state: "required",
						country: "required",
						zip: {
								required: true,
								zipcode: true,
						},
						gradyear: "required",
						LocationID: "required",
						CurriculumID: "required",
				},
				messages: {
						firstname: "Please enter your First Name",
						lastname: "Please enter your Last Name",
						email: "Please enter a valid email address",
						dayphone: "Please enter a valid phone number",
						address: "Please enter your Address",
						city: "Please enter your City",
						state: "Please enter your State",
						zip: "Please enter your Zip Code",
						country: "Please enter your Country",
						gradyear: "Please enter your Graduation Year",
						LocationID: "Please enter your Prefered campus location",
						CurriculumID: "Please specify the program you are intersted in",
				}
		});
});
jQuery(document).ready(function () {
		$(":input").inputmask();
});

$.validator.addMethod('customphone', function (value, element) {
		return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
}, "Please enter a valid phone number");
$.validator.addMethod("fillProgram", function (value, element, param) {
		return this.optional(element) || value != param;
}, "Please specify the program you are intersted in");
$.validator.addMethod("zipcode", function (value, element) {
		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");
$.validator.addMethod("nonNumeric", function (value, element) {
		return this.optional(element) || !value.match(/[0-9]+/);
}, "Only alphabatic characters allowed.");
jQuery().ready(function () {
		// validate signup form on keyup and submit
		$(".contact-form.keypath").validate({
				rules: {
						firstname: "required",
						lastname: "required",
						email: {
								required: true,
								email: true
						},
						dayphone: {
								required: true,
								customphone: true
						},
						address: "required",
						city: {
								required: true,
								nonNumeric: true
						},
						state: "required",
						country: "required",
						zip: {
								required: true,
								zipcode: true,
						},
						gradyear: "required",
						LocationID: "required",
						CurriculumID: "required",
				},
				messages: {
						firstname: "Please enter your First Name",
						lastname: "Please enter your Last Name",
						email: "Please enter a valid email address",
						dayphone: "Please enter a valid phone number",
						address: "Please enter your Address",
						city: "Please enter your City",
						state: "Please enter your State",
						zip: "Please enter your Zip Code",
						country: "Please enter your Country",
						gradyear: "Please enter your Graduation Year",
						LocationID: "Please enter your Prefered campus location",
						CurriculumID: "Please specify the program you are intersted in",
				}
		});
});
jQuery(document).ready(function () {
		$(":input").inputmask();
});

var Curriculum = jQuery("#LocationID");
var select = $("#LocationID").val();
Curriculum.change(function () {
  var select = $("#LocationID").val();
  if (select == '42992') {
    document.ppcForm.CurriculumID.options.length=0
    document.ppcForm.CurriculumID.options[0]=new Option ("Select A Program", "", "true", "true")
    document.ppcForm.CurriculumID.options[1]=new Option ("Baking and Pastry - Associate", "582294")
    document.ppcForm.CurriculumID.options[2]=new Option ("Baking and Pastry - Certificate", "582292")
    document.ppcForm.CurriculumID.options[3]=new Option ("Culinary Arts", "582296")
    document.ppcForm.CurriculumID.options[4]=new Option ("Food and Beverage Management - Associate", "582298")
    document.ppcForm.CurriculumID.options[5]=new Option ("Food and Beverage Management - Bachelor", "582300")
    }

  if (select == '42993') {
    document.ppcForm.CurriculumID.options.length=0
    document.ppcForm.CurriculumID.options[0]=new Option ("Select A Program", "", "true", "true")
    document.ppcForm.CurriculumID.options[1]=new Option ("Baking and Pastry - Associate", "582295")
    document.ppcForm.CurriculumID.options[2]=new Option ("Baking and Pastry - Certificate", "582293")
    document.ppcForm.CurriculumID.options[3]=new Option ("Culinary Arts", "582297")
    document.ppcForm.CurriculumID.options[4]=new Option ("Food and Beverage Management - Associate", "582299")
    }

});

$.validator.addMethod('customphone', function (value, element) {
		return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
}, "Please enter a valid phone number");
$.validator.addMethod("fillProgram", function (value, element, param) {
		return this.optional(element) || value != param;
}, "Please specify the program you are intersted in");
$.validator.addMethod("zipcode", function (value, element) {
		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");
$.validator.addMethod("nonNumeric", function (value, element) {
		return this.optional(element) || !value.match(/[0-9]+/);
}, "Only alphabatic characters allowed.");
jQuery().ready(function () {
		// validate signup form on keyup and submit
		$(".contact-form.keypath").validate({
				rules: {
						firstname: "required",
						lastname: "required",
						email: {
								required: true,
								email: true
						},
						dayphone: {
								required: true,
								customphone: true
						},
						address: "required",
						city: {
								required: true,
								nonNumeric: true
						},
						state: "required",
						country: "required",
						zip: {
								required: true,
								zipcode: true,
						},
						gradyear: "required",
						LocationID: "required",
						CurriculumID: "required",
				},
				messages: {
						firstname: "Please enter your First Name",
						lastname: "Please enter your Last Name",
						email: "Please enter a valid email address",
						dayphone: "Please enter a valid phone number",
						address: "Please enter your Address",
						city: "Please enter your City",
						state: "Please enter your State",
						zip: "Please enter your Zip Code",
						country: "Please enter your Country",
						gradyear: "Please enter your Graduation Year",
						LocationID: "Please enter your Prefered campus location",
						CurriculumID: "Please specify the program you are intersted in",
				}
		});
});
jQuery(document).ready(function () {
		$(":input").inputmask();
});

// Secondary nav sticky function
function stickySecondary() {
	var stickyElement = $('.secondary-nav');
	var stickyHeight = stickyElement.outerHeight();
	var top = stickyElement.offset().top;

	// Recalculate the top offset on page resize so it's crispy smooth still.
	var resizeTimer;
	$(window).on('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			top = stickyElement.offset().top;
		}, 250);
	});

	// The sticky nav binding
	$(window).scroll(function (event) {
		var y = $(this).scrollTop();
		if (y >= top && screenSize(1)) {
			stickyElement.addClass('secondary-nav--sticky');
		} else {
			stickyElement.removeClass('secondary-nav--sticky');
		}
	});
}

// If the secondary nav element exists then trigger secondary nav sticky function
if($('.secondary-nav').length) {
	stickySecondary();
}
$( ".js-calendar-filter" ).datepicker({
	onSelect: function (date) {
		// Update the input text
		updateFeedFilter(date, 'Date');

		// Show the feed filter
		$('.feed-filter').slideDown();

		$('.mobile-filter__overlay').slideUp();
		console.log('Calendar filter selected');
	}
});
// Apply active to popular tag and slide up filter.
$('.popular-tags .popular-tags__tag').on('click',function (e) {
	var tagName = $(this).text();

	e.preventDefault();

	// Remove any other tag that is selected.
	$('.popular-tags__tag--active').removeClass('popular-tags__tag--active');

	// Add active state to selected tag
	$(this).addClass('popular-tags__tag--active');

	// Update the input text
	updateFeedFilter(tagName, 'Tag');

	// Show the feed filter
	$('.feed-filter').slideDown();

	// Slide up mobile filter if it is available
	$('.mobile-filter__overlay').slideUp();

	// For debugging
	console.log('Tag filter selected');
});
$('.campus-filter__form').change(function() {
	var campus = "";
	$( ".campus-filter__form input:checked" ).each(function() {
		campus = $(this).val();
	});

	// Update the input text
	updateFeedFilter(campus, 'Campus');

	// Show the feed filter
	$('.feed-filter').slideDown();

	// Slide up mobile filter if this is mobile.
	$('.mobile-filter__overlay').slideUp();

	// For debugging
	console.log('Campus filter selected');
})

$('.campus-filter__form .campus-filter__input-label').on('click',function (e) {
	$('.campus-filter__form .campus-filter__input-label').removeClass('campus-filter__input-label--active');
	$(this).addClass('campus-filter__input-label--active');
})

$('.feed-filter__input-close').on('click',function (e) {
	$(this).siblings('input').val('');
})

