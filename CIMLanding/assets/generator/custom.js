/** function that returns true based on current screen size
	0 = 0 to 991px
	1 = 992px+
**/
function screenSize(screensize) {
	if ($('#screen-size').css('z-index') >= screensize) {
		return true;
	} else {
		return false;
	}
}


$(document).ready(function(){

	// Only for front end dev debugging
	document.title = $('#pagetitle').text();
	$('body').attr('class', 'template--' + $('#pagetemplate').text());





	$('.js-popup-datepicker').datepicker({
		beforeShow: function(input, inst) {
			$('#ui-datepicker-div').addClass('ui-datepicker--popup');
		}
	});

	function fadeScreen(state){
		if(state == true) {
			$('.fade-screen').addClass('fade-screen--active');
		}
		if(state == false) {
			$('.fade-screen').removeClass('fade-screen--active');
		}
	}





	// .sidebar-left-nav block mobile toggle
	$('.sidebar-left-nav__mobile-toggle').on('click', function(e) {
		e.preventDefault();
		$('.sidebar-left-nav__links').slideToggle('medium', function(){
			$(this).toggleClass('sidebar-left-nav__links--active');
			$(this).removeAttr('style');
		});
	});
	// .sidebar-left-nav block sub menu toggle
	$('.sidebar-left-nav__link-item--has-children .sidebar-left-nav__link').on('click', function(e) {
		e.preventDefault();
			var parent = $(this).parent(),
				currentLink = $(this);
			currentLink.toggleClass('sidebar-left-nav__link--hovered');
		$(this).next('.sidebar-left-nav__sub-nav').slideToggle('medium', function(){
			parent.toggleClass('sidebar-left-nav__link-item--active');
			currentLink.toggleClass('sidebar-left-nav__link--hovered');
		})
	});

	$('.photo-slider__slick').slick({
		slide: '.photo-slider__slide',
		nextArrow: '<button type="button" class="slick-next"></button>',
		prveArrow: '<button type="button" class="slick-prev"></button>',
		dots: true
	});

	$('.card-slider__slick').each(function(idx, item){
		var carouselId = "carousel" + idx;
		this.id = carouselId;
		$(this).slick({
			slide: '#' + carouselId + ' .card-slider__card',
			appendArrows: '#' + carouselId + ' .card-slider__slick-nav',
			slidesToShow: 5,
			dots: false,
			infinite: false,
			variableWidth: true,
			responsive: [
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					variableWidth: true,
				}
			}
			]
		})
	});

	function updateFeedFilter(value, filterType) {
		$('.feed-filter__input').val(value);
		$('.feed-filter__label-type').html(filterType);
	}

	/* blocks js */

	$('select.js-select2').select2({
		minimumResultsForSearch: Infinity
	});


});
