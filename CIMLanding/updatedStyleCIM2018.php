<?php
/**
 * Template Name: New Discover Landing Page 2018 Style
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Discover your way forward at Baker</title>
<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-PH2XSKQ':true});
</script>

 <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TDZV4H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TDZV4H');</script>
<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->


<!-- BEGIN: Marin Software Tracking Script (Landing Page) -->
<script type='text/javascript'>
var _mTrack = _mTrack || [];
_mTrack.push(['trackPage']);

(function() {
var mClientId = '16680mlv57907';
var mProto = (('https:' == document.location.protocol) ? 'https://' : 'http://');
var mHost = 'tracker.marinsm.com';

var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr);
})();
</script>
<noscript>
<img width="1" height="1" src="https://tracker.marinsm.com/tp?act=1&cid=16680mlv57907&script=no" />
</noscript>
<!-- END: Copyright Marin Software -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript” src=”//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->

<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/stylesLandingV1.css"> -->
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDZV4H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
		<div id="DiscoverLandingPage" class="cim">

	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
			 <a href="/" class="main-header__logo">
			  <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Landing page/CIM Logo@2x.png" alt="" class="main-header__logo-img-red">
			  <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Landing page/CIM Logo@2x.png" alt="" class="main-header__logo-img-white">
			 </a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:810-766-2170"></a>
				<a class="main-header__call-text" href="tel:810-766-2170">810-766-2170</a>
			</div>
		</div>
	</div>
</div>

<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php the_field('big_slide_image'); ?>');  background-size: cover; background-position: center;">
<div class="row">
	<div class="LandingPage-fullwidth-video-cta__wrap" style="max-width:  40em;">

      		<h1 class="LandingPage-fullwidth-video-cta__title"><?php the_field('headline'); ?></h1>


		<div class="LandingPage-fullwidth-video-cta__sub-text">
			<p><?php the_field('success_title') ?></p>
		</div>
	</div>
</div>
</div>
<div class="Content">
<div class="row">
	<div class="Content__Right lead-form keypath fixme" id="cform">
		<div class="lead-form_head">
			<h3>Request Info</h3>
		</div>
		<form name="ppcForm" method=get action="https://webservices.keypathpartners.com/ilm/default.ashx" class="contact-form__form keypath">
			<div class="contact-form__error-message">Errors highlighted in red</div>
			<div class="contact-form__required-text">All Fields Required</div>

			<div class="contact-form__row contact-form__row--two">
			<div class="contact-form__field-wrap">
			<input class="text" type='text' value="" name='firstname' id='first-name' placeholder="First Name" required>
			</div>

			<div class="contact-form__field-wrap">
			<input class="text" type='text' name='lastname' value="" id='last-name' placeholder="Last Name" required>
			</div>
			</div>

			<div class="contact-form__row contact-form__row--two">
			<div class="contact-form__field-wrap">
			<input name="email" type="email" value="" name='email' id='email' placeholder="Email" required>
			</div>

			<div class="contact-form__field-wrap">
			<input name="dayphone" type="tel" id='dayphone' value="" placeholder="Phone" data-inputmask="'mask': '999-999-9999'" required>
			</div>
			</div>

			<div class="contact-form__row">
			<div class="contact-form__field-wrap">
			<input class="text" type='text' name='address' id='address' placeholder="Address" required>
			</div>
			</div>
			<div class="contact-form__row">
			<div class="contact-form__field-wrap">
			<input name="city" type="text" id='city' placeholder="City" required>
			</div>
			</div>

			<div class="contact-form__row contact-form__row--two">
			<div class="contact-form__field-wrap">
			<select name="state" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="State" required>
			<option value=""></option>
			<option class="dropdownvalue" value="AL">Alabama</option>
			<option class="dropdownvalue" value="AK">Alaska</option>
			<option class="dropdownvalue" value="AZ">Arizona</option>
			<option class="dropdownvalue" value="AR">Arkansas</option>
			<option class="dropdownvalue" value="CA">California</option>
			<option class="dropdownvalue" value="CO">Colorado</option>
			<option class="dropdownvalue" value="CT">Connecticut</option>
			<option class="dropdownvalue" value="DE">Delaware</option>
			<option class="dropdownvalue" value="DC">Dist. of Col.</option>
			<option class="dropdownvalue" value="FL">Florida</option>
			<option class="dropdownvalue" value="GA">Georgia</option>
			<option class="dropdownvalue" value="HI">Hawaii</option>
			<option class="dropdownvalue" value="ID">Idaho</option>
			<option class="dropdownvalue" value="IL">Illinois</option>
			<option class="dropdownvalue" value="IN">Indiana</option>
			<option class="dropdownvalue" value="IA">Iowa</option>
			<option class="dropdownvalue" value="KS">Kansas</option>
			<option class="dropdownvalue" value="KY">Kentucky</option>
			<option class="dropdownvalue" value="LA">Louisiana</option>
			<option class="dropdownvalue" value="ME">Maine</option>
			<option class="dropdownvalue" value="MD">Maryland</option>
			<option class="dropdownvalue" value="MA">Massachusetts</option>
			<option class="dropdownvalue" value="MI">Michigan</option>
			<option class="dropdownvalue" value="MN">Minnesota</option>
			<option class="dropdownvalue" value="MS">Mississippi</option>
			<option class="dropdownvalue" value="MO">Missouri</option>
			<option class="dropdownvalue" value="MT">Montana</option>
			<option class="dropdownvalue" value="NE">Nebraska</option>
			<option class="dropdownvalue" value="NV">Nevada</option>
			<option class="dropdownvalue" value="NH">New Hampshire</option>
			<option class="dropdownvalue" value="NJ">New Jersey</option>
			<option class="dropdownvalue" value="NM">New Mexico</option>
			<option class="dropdownvalue" value="NY">New York</option>
			<option class="dropdownvalue" value="NC">North Carolina</option>
			<option class="dropdownvalue" value="ND">North Dakota</option>
			<option class="dropdownvalue" value="OH">Ohio</option>
			<option class="dropdownvalue" value="OK">Oklahoma</option>
			<option class="dropdownvalue" value="OR">Oregon</option>
			<option class="dropdownvalue" value="PA">Pennsylvania</option>
			<option class="dropdownvalue" value="RI">Rhode Island</option>
			<option class="dropdownvalue" value="SC">South Carolina</option>
			<option class="dropdownvalue" value="SD">South Dakota</option>
			<option class="dropdownvalue" value="TN">Tennessee</option>
			<option class="dropdownvalue" value="TX">Texas</option>
			<option class="dropdownvalue" value="UT">Utah</option>
			<option class="dropdownvalue" value="VT">Vermont</option>
			<option class="dropdownvalue" value="VA">Virginia</option>
			<option class="dropdownvalue" value="WA">Washington</option>
			<option class="dropdownvalue" value="WV">West Virginia</option>
			<option class="dropdownvalue" value="WI">Wisconsin</option>
			<option class="dropdownvalue" value="WY">Wyoming</option>
			</select>
			</div>
			<div class="contact-form__field-wrap">
				<input name="zip" type="text" id='zip' placeholder="Zip Code" data-inputmask="'mask': '99999'" required>
			</div>
			<!-- <div class="contact-form__field-wrap">
			<label for="country" class="contact-form__label">* Country</label>
			<select name="country" class="js-select2" style="width: 100%" name="state" id="" data-placeholder="Select a Country" required>
			<option value=""></option>
			<option class="dropdownvalue" value="U.S.A.">U.S.A.</option>
			<option class="dropdownvalue" value="Canada">Canada</option>
			<option class="dropdownvalue" value="Outside U.S.A.">Outside U.S.A.</option>
			</select>
			</div> -->
			</div>
			<div class="contact-form__row contact-form__row--two">
				<div class="contact-form__field-wrap">
				<label for="gradyear" class="contact-form__label">* High School Graduation Year:</label>
				<select class="js-select2" style="width: 100%" name="gradyear" id="" data-placeholder="Graduation Year" required>
				<option value=""></option>
				<option class="dropdownvalue" value="2020">2020</option>
				<option class="dropdownvalue" value="2019">2019</option>
				<option class="dropdownvalue" value="2018">2018</option>
				<option class="dropdownvalue" value="2017">2017</option>
				<option class="dropdownvalue" value="2016">2016</option>
				<option class="dropdownvalue" value="2015">2015</option>
				<option class="dropdownvalue" value="2014">2014</option>
				<option class="dropdownvalue" value="2013">2013</option>
				<option class="dropdownvalue" value="2012">2012</option>
				<option class="dropdownvalue" value="2011">2011</option>
				<option class="dropdownvalue" value="2010">2010</option>
				<option class="dropdownvalue" value="2009">2009</option>
				<option class="dropdownvalue" value="2008">2008</option>
				<option class="dropdownvalue" value="2007">2007</option>
				<option class="dropdownvalue" value="2006">2006</option>
				<option class="dropdownvalue" value="2005">2005</option>
				<option class="dropdownvalue" value="2004">2004</option>
				<option class="dropdownvalue" value="2003">2003</option>
				<option class="dropdownvalue" value="2002">2002</option>
				<option class="dropdownvalue" value="2001">2001</option>
				<option class="dropdownvalue" value="2000">2000</option>
				<option class="dropdownvalue" value="1999">1999</option>
				<option class="dropdownvalue" value="1998">1998</option>
				<option class="dropdownvalue" value="1997">1997</option>
				<option class="dropdownvalue" value="1996">1996</option>
				<option class="dropdownvalue" value="1995">1995</option>
				<option class="dropdownvalue" value="1994">1994</option>
				<option class="dropdownvalue" value="1993">1993</option>
				<option class="dropdownvalue" value="1992">1992</option>
				<option class="dropdownvalue" value="1991">1991</option>
				<option class="dropdownvalue" value="1990">1990</option>
				<option class="dropdownvalue" value="1989">1989</option>
				<option class="dropdownvalue" value="1988">1988</option>
				<option class="dropdownvalue" value="1987">1987</option>
				<option class="dropdownvalue" value="1986">1986</option>
				<option class="dropdownvalue" value="1985">1985</option>
				<option class="dropdownvalue" value="1984">1984</option>
				<option class="dropdownvalue" value="1983">1983</option>
				<option class="dropdownvalue" value="1982">1982</option>
				<option class="dropdownvalue" value="1981">1981</option>
				<option class="dropdownvalue" value="1980">1980</option>
				<option class="dropdownvalue" value="1979">1979</option>
				<option class="dropdownvalue" value="1978">1978</option>
				<option class="dropdownvalue" value="1977">1977</option>
				<option class="dropdownvalue" value="1976">1976</option>
				<option class="dropdownvalue" value="1975">1975</option>
				<option class="dropdownvalue" value="1974">1974</option>
				<option class="dropdownvalue" value="1973">1973</option>
				<option class="dropdownvalue" value="1972">1972</option>
				<option class="dropdownvalue" value="1971">1971</option>
				<option class="dropdownvalue" value="1970">1970</option>
				<option class="dropdownvalue" value="1969">1969</option>
				<option class="dropdownvalue" value="1968">1968</option>
				<option class="dropdownvalue" value="1967">1967</option>
				<option class="dropdownvalue" value="1966">1966</option>
				<option class="dropdownvalue" value="1965">1965</option>
				<option class="dropdownvalue" value="1964">1964</option>
				<option class="dropdownvalue" value="1963">1963</option>
				<option class="dropdownvalue" value="1962">1962</option>
				<option class="dropdownvalue" value="1961">1961</option>
				<option class="dropdownvalue" value="1960">1960</option>
				<option class="dropdownvalue" value="1959">1959</option>
				<option class="dropdownvalue" value="1958">1958</option>
				<option class="dropdownvalue" value="1957">1957</option>
				<option class="dropdownvalue" value="1956">1956</option>
				<option class="dropdownvalue" value="1955">1955</option>
				</select>
			</div>
				<div class="contact-form__field-wrap">
				<label for="LocationID2" class="contact-form__label">*  Desired Campus Location</label>
				<select class="js-select20ff" style="width: 100%" name="LocationID" id="LocationID" data-placeholder="Choose a Location" required>
				<option value=""></option>
				</select>
				</div>
			</div>
			<div class="contact-form__row">
				<div class="contact-form__field-wrap">
				<label for="CurriculumID" class="contact-form__label">* Desired Program</label>
				<select class="js-select2Off" style="width: 100%" name="CurriculumID" id="CurriculumID" data-placeholder="Select a Program" required>
				<option value=""></option>
				</select>
				</div>
			</div>
			<input name="" type="submit" class="btn-cta-round btn-cta-round--red contact-form__submit" value="Submit">

			<input type='hidden' placeholder="" name='FormID' id='FormID' value=7455 >
			<input type='hidden' placeholder="" name='CampaignID' id='CampaignID' value=7168 >
			<input type='hidden' placeholder="" name='AffiliateLocationID' id='AffiliateLocationID' value=0 >
			<input type='hidden' placeholder="" name='VendorID' id='VendorID' value=38999 >

<div class="hideme" style="display: none !important;">
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
    <input type='hidden' placeholder="" name='CaptureURL' id='CaptureURL' value='<?php echo "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'>
    <input type='hidden' placeholder="" name='ReturnToURL' id="ReturnToURL" value="https://discover.culinaryinstitutemi.com/here-for-the-making/thank-you/">
    <input type="hidden" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>">
    <input type="hidden" name="SearchEngine" id="SearchEngine" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="SearchEngineCampaign" id="SearchEngineCampaign" value="<?php echo $_REQUEST['SearchEngine']?>">
    <input type="hidden" name="VendorAccountID" id="VendorAccountID" value="<?php echo $_REQUEST['VendorAccountID']?>">
    <input type='hidden' placeholder="" name='ClientSourceCode' id='ClientSourceCode' value="<?php echo $_REQUEST['ClientSourceCode']?>" >
	<?php endwhile; ?>
<?php endif; ?>
</div>



			<script src="//webservices.plattformad.com/cfe/JSON.ashx?JSONAction=FormProgramsExtended&formid=7455&CurriculumCategoryID=8" type="text/javascript"></script>
			<script src="//webservices.plattformad.com/cfe/scripts/JSONObjectCurriculumHandlerCID.js" type="text/javascript"></script>
			<script src="//tracking.plattformad.com/" type="text/javascript"></script>
			<script type="text/javascript">
			var __ProgramPleaseSelectText = "Select a program";
			var __LocationPleaseSelectText = "Location of interest";

			InitCurriculumDropDown('LocationID','CurriculumID');
			</script>

			<script src="//tracking.plattformad.com/LeadSourceRuntime.aspx?&&REFERER_SITE=https://discover.culinaryinstitutemi.com" language="javascript"></script>
			<script src="//artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js" type="text/javascript"></script>



			<!--<script type="text/javascript" src="https://tracking.plattformad.com/"></script>
			<script type="text/javascript" src="https://artifacts.plattformad.com/repository/scripts/SEOKeywordEngineTracking.js"></script>-->




			<script>

			$.validator.addMethod('customphone', function (value, element) {
			  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
			}, "Please enter a valid phone number");

			$.validator.methods.email = function( value, element ) {
	  	return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
			}

			$.validator.addMethod("fillProgram", function(value, element, param) {
			  return this.optional(element) || value != param;
			}, "Please specify the program you are interested in");

			$.validator.addMethod("zipValid", function(value, element) {
			  return this.optional(element) || /^\d{5}$/.test(value);
			}, "Please provide a valid zipcode.");

			$.validator.addMethod("nonNumeric", function(value, element) {
			  return this.optional(element) || !value.match(/[0-9]+/);
			},"Only alphabatic characters allowed.");

			Inputmask.extendDefaults({
			  'autoUnmask': true
			});

			$().ready(function() {
				// validate signup form on keyup and submit
				$(".keypath").validate({
					rules: {
						firstname: "required",
						lastname: "required",
						email: {
							required: true,
						},
						dayphone: "customphone",
						address: "required",
						city: {
							required: true,
							nonNumeric: true
						},
						state: "required",
						zip: "zipValid",
						gradyear: "required",
						LocationID: "required",
						CurriculumID: "required",
					},
					messages: {
						firstname: "Please enter your First Name",
						lastname: "Please enter your Last Name",
						email: "Please enter a valid email address",
						dayphone: "Please enter a valid phone number",
						address: "Please enter your Address",
						city: "Please enter your City",
						state: "Please enter your State",
						zip: "Please enter your Zip Code",
						gradyear: "Please enter your Graduation Year",
						LocationID: "Please enter your Preferred campus location",
						CurriculumID: "Please specify the program you are interested in",
					}
				});
			});

			</script>
		</form>
	</div>
	<div class="Content__Left">
		<?php if(get_field('video')): ?>
			<div class="video_holder"><iframe src="https://www.youtube.com/embed/<?php the_field('video') ?>" frameborder="0" allowfullscreen></iframe></div>
		<?php endif; ?>
		<div class="success_holder">
	      <?php the_field('sucess_starts_here_para') ?>
	  </div>
 <h3>Here for the <span class="red">making.</span></h3>

		<div class="locations-grid">
			<h5>For Your Convenience</h5>
			<h3 class="title">Our Campuses</h3>
			<div class="row">
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>CIM-Muskegon</h3></div>
					<div class="locations-grid__location-address">1903 Marquette Ave<br>Muskegon, MI 49442</div>
					<a href="https://www.google.com/maps/place/1903+Marquette+Ave,+Muskegon,+MI+49442/@43.2443111,-86.1987796,17z/data=!3m1!4b1!4m5!3m4!1s0x88197ceb23c7c5ff:0x2bbf2293dc2c7892!8m2!3d43.2443111!4d-86.1965856" target="_blank">Get Directions</a>
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>CIM-Port Huron</h3></div>
					<div class="locations-grid__location-address">3402 Lapeer Road<br>Port Huron, MI 48060</div>
					<a href="https://www.google.com/maps/place/3402+Lapeer+Rd,+Port+Huron,+MI+48060/@42.9791066,-82.4702114,17z/data=!3m1!4b1!4m5!3m4!1s0x88259c26a075a79f:0x75bc71d307dc6855!8m2!3d42.9791066!4d-82.4680174" target="_blank">Get Directions</a>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<?php the_field('extratracking_code'); ?>

<!-- BEGIN: Marin Software Tracking Script (Landing Page) -->
<script type='text/javascript'>
var _mTrack = _mTrack || [];
_mTrack.push(['trackPage']);

(function() {
var mClientId = '16680mlv57907';
var mProto = (('https:' == document.location.protocol) ? 'https://' : 'http://');
var mHost = 'tracker.marinsm.com';

var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr);
})();
</script>
<noscript>
<img width="1" height="1" src="https://tracker.marinsm.com/tp?act=1&cid=16680mlv57907&script=no" />
</noscript>
<!-- END: Copyright Marin Software -->

<div class="programs">
	<div class="row">
		<div class="programs-title">
		<h5>Explore Your Future</h5>
		<h2><?php the_field('degree_name'); ?></h2>
		</div>
		<?php if( get_field('how_may_columns_of_programs') == '1' ): ?>
		<div class="programs-Programs">
			<div class="center">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_1_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_1_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
					<?php the_field('degree_program_1_programs'); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if( get_field('how_may_columns_of_programs') == '2' ): ?>
		<div class="programs-Programs">
			<div class="left">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_1_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_1_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
					<?php the_field('degree_program_1_programs'); ?>
				</div>
			</div>
			<div class="right">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_2_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_2_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
						<?php the_field('degree_program_2_programs'); ?>
				</div>
		</div>
	</div>
	<?php endif; ?>
</div>
</div>

<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="<?php echo get_bloginfo('template_directory');?>/slices/CIMMarkWhiteonBlack.png"/>
		</div>
		<p class="footer-section__accredited">Accredited by <span class="footer-section__link_underline"><a href="#">The Higher Learning Commission</a></span>. An  equal opportunity affirmative action institution.
		An approved institution of the <span class="footer-section__link_underline"><a href="#">National Council for State Authorization Reciprocity Agreements (NC-SARA)</a></span>
		and the <span class="footer-section__link_underline"><a href="#">Midwestern Higher Education Compact (MHEC)</a></span>
		</p>
		<div class="footer-section__bottom_rights-reserved"><a href="http://www.baker.edu">Visit baker.edu</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;© <?php echo date("Y"); ?> All rights reserved. Baker College</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js?ver=1.1'></script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/zip.js" type="text/javascript" /></script>

		<!-- <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js" type="text/javascript"></script>
		<script src="dist/js/jquery.validate.js" type="text/javascript"></script>
		<script src="dist/js/additional-methods.js" type="text/javascript"></script> -->


		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<script>

		//Follow form Js
		$(window).on('resize', function () {
		    $('.Thank .head').css('height', $(this).height() + "px");
		    formScroll();
		});

		function formScroll() {
		  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
		  var fixmePosition = $('.fixme').position();
		  var leftmargin = $('.fixme').css('margin-left');
		  var formwidth = document.getElementById('cform').offsetWidth;
		  //var intoSize = $('.intro').height() + 35;
		  var intoSize = $('#intro').height();
		  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

		  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
			console.log(collapsePoint);

		  //$(window).scroll(function() {
		  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
		        var currentScroll = $(window).scrollTop();        // get current position
		        var windowSize = $(window).width();
		        var formHeight= $('.fixme').height();
		        /*var tilesHeight= $('.tiles').offset().top ;
		        var stopMe = tilesHeight - formHeight;*/
		        var tilesHeight= $('.locations').offset().top ;
		        var stopMe = tilesHeight - formHeight;


		        	if (currentScroll >= (fixmeTop + 108) && windowSize > 1000) {    // apply position: fixed if you
		            var fromTop = $('.fixme').offset().top;
		            $('.fixme').css({                                 // scroll to that element or below it
				          position: 'fixed',
				          top: '-108px',
				          left: (fixmePosition.left) + 'px',
									width: formwidth,
									marginLeft: '0',
				          marginTop: '-' + intoSize + 'px',
				          });
				        }
			        else {                               // apply position: static
			        $('.fixme').css({                      // if you scroll above it
								position:'',
								left :'',
								top: '',
								marginTop:'',
								marginLeft: '',
								width:'',
			          });
			        }

			        if (currentScroll >= (stopMe + 108) && windowSize > 1000) {           // apply position: fixed if you
		              $('.fixme').css({                                 // scroll to that element or below it
		                  position: 'absolute',
		                  /*width: width + 'px',*/
		                  top: stopMe + 'px',
		                  left: fixmePosition.left + 'px',
		                  marginTop: '-' + intoSize + 'px',
		                });
			        	}
								if (windowSize < 1000){
									$('.fixme').css({                                 // scroll to that element or below it
										 position: '',
										 /*width: width + 'px',*/
										 top: '',
										 left: '',
											 marginTop: '20px',
									 });
								}

				  	});
				//console.log (leftmargin);
				}


		$(document).ready(function(){
    	formScroll();
		});

		$.validator.addMethod('customphone', function (value, element) {
		  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
		}, "Please enter a valid phone number");

		$.validator.methods.email = function( value, element ) {
  	return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
		}

		$.validator.addMethod("fillProgram", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify the program you are interested in");

		$.validator.addMethod("zipValid", function(value, element) {
		  return this.optional(element) || /^\d{5}$/.test(value);
		}, "Please provide a valid zipcode.");

		$.validator.addMethod("nonNumeric", function(value, element) {
		  return this.optional(element) || !value.match(/[0-9]+/);
		},"Only alphabatic characters allowed.");

		Inputmask.extendDefaults({
		  'autoUnmask': true
		});

		$().ready(function() {
			// validate signup form on keyup and submit
			$(".keypath").validate({
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
					},
					dayphone: "customphone",
					address: "required",
					city: {
						required: true,
						nonNumeric: true
					},
					state: "required",
					zip: "zipValid",
					gradyear: "required",
					LocationID: "required",
					CurriculumID: "required",
				},
				messages: {
					firstname: "Please enter your First Name",
					lastname: "Please enter your Last Name",
					email: "Please enter a valid email address",
					dayphone: "Please enter a valid phone number",
					address: "Please enter your Address",
					city: "Please enter your City",
					state: "Please enter your State",
					zip: "Please enter your Zip Code",
					gradyear: "Please enter your Graduation Year",
					LocationID: "Please enter your Preferred campus location",
					CurriculumID: "Please specify the program you are interested in",
				}
			});
		});
		$('.Programs > h2').append(' <i>  </i>');
		$('.Programs > h3').append(' <i>  </i>');
    $('.Programs > h3').click(function(e) {
        var current = $(this).next('blockquote');
        $('.Programs > blockquote').not(current).hide(100);
        $('.Programs > h3 i').not($(this).find('i')).removeClass('open_arrow');
        $('.Programs > h3').not($(this)).removeClass('open');
        current.slideToggle();
        if ($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open')) {
            $(this).find('i').removeClass('open_arrow');
            $(this).removeClass('open');
        } else if (!$(this).find('i').hasClass('open_arrow') || !$(this).hasClass('open')) {
            $(this).find('i').addClass('open_arrow');
            $(this).addClass('open')
        }
        if ($(window).width() < 767) {}
    });
		function formScroll() {
			var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
		  var fixmePosition = $('.fixme').position();
		  var leftmargin = $('.fixme').css('margin-left');
		  var formwidth = document.getElementById('cform').offsetWidth;
		  //var intoSize = $('.intro').height() + 35;
		  var intoSize = $('#intro').height();
			var windowSize = $(window).width();
			var leadFormHeight= $('.contact-form__form').outerHeight();
		  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

		  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
			console.log(collapsePoint);


			if (windowSize < 991){
					 $( ".lead-form_head" ).unbind('click').click(function(){
						 if ($('.lead-form_head').hasClass('closed')) {
							$( '.fixme' ).animate({top: '0', bottom: '0'});
							$( '.lead-form_head' ).removeClass('closed');
							$( '.lead-form_head' ).addClass('open');
						}
						 else {
							$( '.fixme' ).animate({ bottom: '-' + leadFormHeight + 'px' });
							$( '.fixme' ).css({ top:'' });
							$( '.lead-form_head' ).removeClass('open');
							$( '.lead-form_head' ).addClass('closed');
						}
					});

				}
		//console.log (leftmargin);
		}


	$(document).ready(function(){
			formScroll();
	var windowSize = $(window).width();
	if (windowSize < 991){
	var leadFormHeight= $('.contact-form__form').outerHeight();
		$( '.lead-form_head' ).addClass('closed');
		$('.fixme').css({      // scroll to that element or below it
			position: 'fixed',
			/*width: width + 'px',*/
			top: 'unset',
			left: '',
			margin: '0 -20px',
			bottom: '-' + leadFormHeight + 'px',
		 });
		}
	});
		</script>

		</body>
</html>
