<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test custom post type -->

<div class="head" style="background: <?php the_field('big_slide_image'); ?> ;">
<!-- Slide/video splash -->
<div id="top">
  <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
      </div><!-- close logo-->
    <div id="phone">
      <p style="font-family: Nexa-regular, sans-serif;"><span id="" style="width: 100%; float: left;">Toll-Free:</span> <a href="tel:855-487-7888" style="color: #303842;">855-487-7888</a></p>
    </div><!-- close phone-->
  </div><!--close top -->
<header>
  <div id="SlideInfo">
	   <div class="VertAlign">
    	  <h1><?php the_field('headline'); ?></h1>
       		<?php if(get_field('play_button_url_')): ?>
		 <a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/slices/Video Ply BTN.png" alt="Play Video"></a>
		<?php endif; ?>

        <p><?php the_field('sub-headline') ?></p>
	    </div>
    </div><!-- close SlideInfo -->
  </header>

</div><!-- close head -->





<!-- mobile form -->

<div class="mobileForm">

<span class="anchor" id="mobileForm"></span>

    <div id="Intro">

      <h4><?php the_field('form_title'); ?></h4>

      <p><?php the_field('form_content'); ?></p>

    </div>

    <?php get_template_part('keypathFormMobile'); ?>

</div>



<div class="goToFormMobile">

   <a href="#mobileForm"><div id="goToFormMobile">

     <h4><?php the_field('form_title'); ?></h4>

     <p>Click here to request information</p>

   </div></a>

</div>





<div id="middle">

<div class="content">



<!-- Follow form section -->

  <main>
    <h2 class="Sucess" style="margin-top: 65px;">Success Starts <span class="Blue">Here</span></h2>
      <?php the_field('sucess_starts_here_para') ?>

      <h5>5 Reasons to enroll</h5>

      	<?php the_field('reasons_to_enroll') ?>

      <h2>Available degrees</h2>

	<span><?php the_field('avail_degrees') ?></span>



      <div class="hold">

        <img class="left" src="<?php the_field('1st_program_image') ?>" />

        <h3><?php the_field('1st_program_title'); ?></h3>

        <?php the_field('1st_program_information'); ?>

      </div>



	<?php if(get_field('2nd_program_title')): ?>

        	<div class="hold">

			<img class="right" src=" <?php the_field('2nd_program_image')?>" />

			<h3><?php the_field('2nd_program_title')?> </h3>

			<?php the_field('2nd_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('3rd_program_title')): ?>

        	<div class="hold">

			<img class="left" src="<?php the_field('3rd_program_image')?>" />

			<h3><?php the_field('3rd_program_title')?> </h3>

			<?php the_field('3rd_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('4th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('4th_program_image')?>" />

			<h3><?php the_field('4th_program_title')?> </h3>

			<?php the_field('4th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('5th_program_title')): ?>

        	<div class="hold">

			<img class="left" src="<?php the_field('5th_program_image')?>" />

			<h3><?php the_field('5th_program_title')?> </h3>

			<?php the_field('5th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('6th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('6th_program_image')?>" />

			<h3><?php the_field('6th_program_title')?> </h3>

			<?php the_field('6th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('7th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('7th_program_image')?>" />

			<h3><?php the_field('7th_program_title')?> </h3>

			<?php the_field('7th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('8th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('8th_program_image')?>" />

			<h3><?php the_field('8th_program_title')?> </h3>

			<?php the_field('8th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('9th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('9th_program_image')?>" />

			<h3><?php the_field('9th_program_title')?> </h3>

			<?php the_field('9th_program_information')?>

		</div>

	<?php endif; ?>

	<?php if(get_field('10th_program_title')): ?>

        	<div class="hold">

			<img class="right" src="<?php the_field('10th_program_image')?>" />

			<h3><?php the_field('10th_program_title')?> </h3>

			<?php the_field('10th_program_information')?>

		</div>

	<?php endif; ?>

  </main>



  <!-- Follow form -->

  <div class="aside fixme" id="fix" >

      <div id="Intro" class="intro">

        <h4><?php the_field('form_title'); ?></h4>

        <p><?php the_field('form_content'); ?></p>

      </div>

	<?php get_template_part('keypathForm'); ?>



</div>

</div><!-- .content close -->

</div>



<!-- Photo/Instagram tile area-->

<div class="tiles">

  <div id="section">

    <div class="contain">
      <h2>Find <span class="Blue">Your People</span></h2>
<p>Join the thousands of Baker students and Alumni that are enjoying their well deserved success. Being proudly passionate about something can make you feel like an anomaly, but this is a community of people who are just as dedicated as you are. The meaningful relationships you make here will go far beyond graduation.</p>

      <h6>#IAMADI</h6>

    </div>

  </div>

</div>

<!-- CTA -->

<div class="footerSplash">

  <footer>

    <div class="contain">

	<h2>Get <span>Started Today</span></h2>
	<!--<img src="<?php echo get_bloginfo('template_directory');?>/slices/GET_STARTED_TODAY.png"/>-->

	<a href="<?php the_field('cal_to_action_link') ?>" class="button">GET STARTED</a>

    </div>

  </footer>

</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
